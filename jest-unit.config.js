module.exports = {
    globals: {
        "ts-jest": {
            tsconfig: "tsconfig.json",
        },
    },
    setupFiles: ["dotenv/config"],
    moduleFileExtensions: ["js", "ts"],
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    testRegex: "./test/unit/.*.ts",
    testEnvironment: "node",
    coverageReporters: ["text", "cobertura"],
    collectCoverageFrom: ["./src/**/*.ts", "!./src/**/*.d.ts"],
};
