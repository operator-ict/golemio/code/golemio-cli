# Release management

Toto je nápověda k cli příkazu `release`, který se snaží automatizovat proces popsaný zde ([core/docs/release_management_CZ](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/docs/release_management_CZ.md)).

> Následující popis počítá s nastavením env:
>
> `RELEASE_BRANCH_NAME=release`
>
> `TARGET_BRANCH_NAME=master`


## Označení release kandidátů (`check`)

Na začátku pomocí příkazu `rls check --scope <scope>` vývojář projde diff a changelog všech modulů a dle uvážení označí daný modul jako Release Candidate (čti přidání id modulu do souboru `tmp/release-candidates_<scope>.json`).

Lze používat přepínače --only --skip nebo --interactive.

Přepínač --scope určuje, které repozitáře mají být procházeny, tzn. `core`, `modules`, `services`, `web-datapraha`, `web-golemiobi`, `web-golemiocz`, `web-lkod`, `web-vymi`.


## Merge release kandidátů (`merge`)

Zavoláním příkazu `rls merge --scope <scope>` se spustí proces postupného vytváření merge requestů.

Pro každý modul zvlášť je potřeba provést tyto akce:
- bump verze
  - do konzole jsou vypsány verze z master a release větve
  - vývojář vybere dle changelogu nebo uvážení správnou verzi a vyplní ji
- aktualizace changelogu
  - automaticky se do changelogu doplní "## [verze] - datum"
- (volitelně) upgrade @golemio závislostí
  - vývojář je vyzván, pokud dá ano, tak se spustí `npm install -E $(npm ls -p --depth=0 | grep -o '@golemio.*' | sed 's/$/@latest/' | sed 's/\\/\//g')`

Po tomto se automaticky commitnou změny, vytvoří se merge request a otevře se okno prohlížeče s odkazem na MR.

Přes api nejde mergovat, takže merge musí být proveden v prohlížeči.

## Kontrola stavu pipeline (`check-mr-pipelines`)

Zavoláním příkazu `rls check-mr-pipelines --scope <scope>` lze kontrolovat zda už doběhly pipeliny. Kontrolují se jak MR pipeliny, tak následně mergnuté pipeliny.

Stav pipeline se vypíše do konzole. Nic víc to nedělá.

## Synchronizace větví (`sync-branches`)

Zavoláním příkazu `rls sync-branches --scope <scope>` se provede "back-merge" aktuálního stavu v master větvi zpět do release a development větví.

Bump verze se provádí pouze v development větvi.

Pokud se objeví konflity, vypíše se warning do konzole s tím, že back-merge je potřeba udělat ručně.

## Vytvoření tagu a releasu (`create-tags`)

Zavoláním příkazu `rls create-tags --scope <scope>` se vytvoří v master větvi nový tag a z něj nový release.

Release message se automaticky vyparsuje z changelogu.

## (volitelně) Kontrola konstant (`check-modules-integrity`)

Zavoláním příkazu `rls check-modules-integrity` se zkontroluje jestli náhodou nechybí nějaký modul v konstantách (src/constants). Porovnání probíhá oproti celé skupině (group) golemio/code/modules.

## (volitelně) Výpis pro release issue (`print-release-issue`)

Zavoláním příkazu `rls print-release-issue` se do konzole vypíše seznam issue a tabulka release kandidátů ve formátu markdown. Tento výpis se dá jednoduše zkopírovat a vložit do šablony issue. Tento příkaz je vhodné volat před příkazem `merge`.
