# Database Migrations

A valid npm lockfile (package-lock.json or npm-shrinkwrap.json) must be present to run database migrations.

## Default Module Schema Migrations

Suppose a module **A**, and other modules (**B**, **C**) depend on module **A**. Each of these modules defines its own database
migrations. Suppose you want the database migrations in module **A** to be included in both database schemas of the dependent
modules **B** and **C**.

This can be achieved by using a module **A** migrations directory with a `.config.json` containing
```json
{ "schema": "$DEFAULT_MODULE_PG_SCHEMA" }
```
where the `$DEFAULT_MODULE_PG_SCHEMA` string will be replaced by default module schemas of the dependant modules **B** and **C**
when running the `migrate` commands (each schema will generate a separate migration).

The default module schema of a module (such as module **B** and **C**) is then defined by including the property
`"isSchemaModuleDefault": true` in the `.config.json` (along with the `schema` property) of select migration of these modules.

The migrations for these modules must be executed in one `migrate` command (by specifying `POSTGRES_MIGRATIONS_DIR` variable with
a multi-directory glob).

## Multi-directory Single-schema Migrations

Migrations using multiple migration directories targeting one database schema are considered *multi-directory single-schema
migrations*. An example of such migrations are *default module schema migrations* described above, but it also includes migrations
not using schema defaults.

It is important that these migrations across different migration directories do not depend on each other when migrating at once
using a multi-directory `POSTGRES_MIGRATIONS_DIR` glob, as the directory-agnostic order of execution is not guaranteed across the
`up` and `down` migrate commands.
