import glob from "fast-glob";
import fs from "fs";
import { GluegunCommand } from "gluegun";
import isGlob from "is-glob";
import path from "path";
import {
    CommandResult,
    getDefaultModulePgSchemas,
    getPostgresMigrateInstances,
    printCommandHelp,
    printSummaryMatrix,
    readLockfile,
} from "../utils/migrate-db.utils";

const command: GluegunCommand = {
    description: "Run database migrations",
    alias: "mdb",
    run: async ({ parameters, prompt, print, strings }) => {
        let [action, migrationName] = parameters.array!;
        let { postgres: isPostgres, schema: postgresSchema, silent: isSilent, path: migrationsDirPath } = parameters.options;

        const postgresConfig = {
            connectionString: process.env.POSTGRES_CONN ?? "",
            migrationsDirPath: migrationsDirPath
                ? Array.isArray(migrationsDirPath)
                    ? migrationsDirPath.map((el) => path.resolve(process.cwd(), el))
                    : [path.resolve(process.cwd(), migrationsDirPath)]
                : [path.resolve(process.cwd(), process.env.POSTGRES_MIGRATIONS_DIR ?? "db/migrations/postgresql")],
        };

        if (!isPostgres) {
            isPostgres = true;
        }

        switch (action) {
            case "help":
            case "h":
            default:
                printCommandHelp();
                break;

            case "create":
                while (!migrationName) {
                    const { newMigrationName } = await prompt.ask([
                        {
                            type: "input",
                            name: "newMigrationName",
                            message: "Specify the migration name",
                        },
                    ]);

                    migrationName = strings.kebabCase(newMigrationName.trim());
                }

                if (isPostgres) {
                    if (postgresConfig.migrationsDirPath.length > 1 || isGlob(postgresConfig.migrationsDirPath[0])) {
                        print.error("Multiple paths / globs are not supported for this action");
                        process.exitCode = 1;
                        return;
                    }

                    const { migrations, createLocalConfig } = getPostgresMigrateInstances({
                        connectionString: postgresConfig.connectionString,
                        defaultSchema: postgresSchema,
                        migrationDir: postgresConfig.migrationsDirPath[0],
                    });

                    for (const migration of migrations) {
                        await migration.create(migrationName).then(createLocalConfig);
                    }
                }

                break;

            case "up":
            case "down":
            case "reset":
                if (isPostgres) {
                    if (!postgresConfig.connectionString) {
                        print.error("Environmental variable POSTGRES_CONN is not defined");
                        process.exitCode = 1;
                        return;
                    }

                    const migrationDirs: string[] = [];

                    for (const p of postgresConfig.migrationsDirPath) {
                        // migrationsDirPath is glob, resolve all directories
                        if (isGlob(p)) {
                            for (const dir of glob.sync(p, {
                                onlyDirectories: true,
                                absolute: true,
                            })) {
                                migrationDirs.push(dir);
                            }
                        }
                        // migrationsDirPath is path, check
                        else {
                            if (!fs.existsSync(p) || !fs.lstatSync(p).isDirectory()) {
                                print.warning(`Migrations directory ${p} does not exist`);
                            } else {
                                migrationDirs.push(p);
                            }
                        }
                    }

                    if (migrationDirs.length < 1) {
                        print.error("Zero migration directories");
                        process.exitCode = 1;
                        return;
                    }

                    let results: CommandResult[] = [];

                    const stdoutWriteStream = process.stdout.write;
                    if (isSilent) {
                        process.stdout.write = () => true;
                    }

                    // Workaround to suppress EventEmitter memory leak warnings (required for db-migrate)
                    // TODO think of a better solution, like spawning multiple processes with child_process.fork
                    if (migrationDirs.length >= process.getMaxListeners()) {
                        process.setMaxListeners(migrationDirs.length + 1);
                    }

                    const defaultModulePgSchemas = await getDefaultModulePgSchemas(migrationDirs);
                    const lockfileData = readLockfile();

                    const migrateInstances: Array<{ migration: any; migrationDir: string; schema: string }> = [];
                    for (const dir of migrationDirs) {
                        const { migrations, schemas } = getPostgresMigrateInstances({
                            connectionString: postgresConfig.connectionString,
                            migrationDir: dir,
                            defaultModulePgSchemas,
                            defaultSchema: postgresSchema,
                            includeLocalConfig: true,
                            lockfileData,
                        });
                        for (let i = 0; i < migrations.length; i++) {
                            migrateInstances.push({ migration: migrations[i], migrationDir: dir, schema: schemas[i] });
                        }
                    }

                    for (const { migration, migrationDir, schema } of migrateInstances) {
                        migration.silence(true);
                        try {
                            await migration[action]();
                            results.push([migrationDir, schema, true]);
                        } catch (err) {
                            console.error(
                                print.colors.red(
                                    `Migration error at ${path.relative(process.cwd(), migrationDir)}: ${err.message}`
                                )
                            );

                            results.push([migrationDir, schema, false, err.message]);
                            process.exitCode = 1;
                            break;
                        }
                    }

                    process.stdout.write = stdoutWriteStream;

                    // Print summary table
                    printSummaryMatrix(results);

                    // db-migrate sometime fails to destroy all connections, this is recommended
                    process.exit();
                }
        }
    },
};

export default command;
