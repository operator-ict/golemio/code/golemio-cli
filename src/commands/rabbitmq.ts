import { GluegunCommand } from "gluegun";
import { printCommandHelp, printCommandHelpReceive, printCommandHelpSend, receiveData, sendData } from "../utils/rabbitmq.utils";

const command: GluegunCommand = {
    description: "Send/receive data to RabbitMQ",
    alias: "rmq",
    run: async ({ parameters, print }) => {
        const [action] = parameters.array!;
        const { help, exchange: exchangeName, queueKey, messageOrPathToFile, queueName, dequeue } = parameters.options;

        if (!process.env.RABBIT_CONN) {
            print.error("Environmental variable RABBIT_CONN is not defined");
            process.exitCode = 1;
            return;
        }

        switch (action) {
            case "help":
            case "h":
            default:
                printCommandHelp();
                break;

            case "send":
            case "s":
                if (help) {
                    printCommandHelpSend();
                    break;
                }

                if (!exchangeName) {
                    print.error("Specify exchange. E.g. 'dataplatform'");
                    process.exitCode = 1;
                }
                if (!queueKey) {
                    print.error("Specify queueKey. E.g. 'manual.dataplatform.test.queue'");
                    process.exitCode = 1;
                }
                if (!messageOrPathToFile) {
                    print.error("Specify messageOrPathToFile. E.g. 'Some data' or '/path/to/file.txt'");
                    process.exitCode = 1;
                }

                await sendData({ exchangeName, queueKey, messageOrPathToFile });

                break;

            case "receive":
            case "r":
                if (help) {
                    printCommandHelpReceive();
                    break;
                }

                if (!queueName) {
                    print.error("Specify queueName. E.g. 'dataplatform.test.queue'");
                    process.exitCode = 1;
                }

                const isDequeue = dequeue && dequeue === "true";

                await receiveData({ queueName, dequeue: isDequeue });

                break;
        }
    },
};

export default command;
