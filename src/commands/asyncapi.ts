import { GluegunCommand } from "gluegun";
import { generateAsyncApiConfig, uploadFile } from "../utils/asyncapi.utils";
import { printCommandHelp } from "../utils/asyncapi.utils";

const command: GluegunCommand = {
    description: "Generate asyncapi document",
    alias: "asp",
    run: async ({ parameters, print }) => {
        const [action] = parameters.array!;
        let {
            header: aspsHeaderFilePath,
            output: aspsMergedOutputFilePath,
            asps: aspsFilePath,
            input: fileInput,
        } = parameters.options;

        const asyncapiConfig = {
            aspsHeaderFilePath: aspsHeaderFilePath ?? null,
            aspsFilesGlob: aspsFilePath ?? null,
            aspsMergedOutputFilePath: aspsMergedOutputFilePath ?? null,
            fileInputPath: fileInput ?? null,
        };

        switch (action) {
            case "help":
            case "h":
            default:
                printCommandHelp();
                return;

            case "merge": {
                if (!asyncapiConfig.aspsMergedOutputFilePath || !asyncapiConfig.aspsFilesGlob) {
                    print.error("Missing required parameters");
                    process.exitCode = 1;
                    return;
                }

                const { isError, message } = await generateAsyncApiConfig(
                    asyncapiConfig.aspsHeaderFilePath,
                    asyncapiConfig.aspsFilesGlob,
                    asyncapiConfig.aspsMergedOutputFilePath
                );

                if (isError) {
                    print.error(message);
                    process.exitCode = 1;
                } else {
                    print.success(message);
                }

                break;
            }

            case "upload": {
                if (!asyncapiConfig.aspsMergedOutputFilePath || !asyncapiConfig.fileInputPath) {
                    print.error("Missing required parameters");
                    process.exitCode = 1;
                    return;
                }

                const { isError, message } = await uploadFile(
                    asyncapiConfig.fileInputPath,
                    asyncapiConfig.aspsMergedOutputFilePath
                );

                if (isError) {
                    print.error(message);
                    process.exitCode = 1;
                } else {
                    print.success(message);
                }

                break;
            }
        }
    },
};

export default command;
