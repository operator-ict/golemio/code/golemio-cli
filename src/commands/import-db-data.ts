import { GluegunCommand } from "gluegun";
import { importPostgresData, extractDangerousWords } from "../utils/import-db-data.utils";

const command: GluegunCommand = {
    description: "Import data to database",
    alias: "idd",
    run: async ({ parameters, print }) => {
        const [action] = parameters.array!;
        let { postgres: isPostgres, dangerous: isDangerous, schema } = parameters.options;

        if (!isPostgres) {
            isPostgres = true;
        }

        if (action === "help" || action === "h") {
            print.info(print.colors.blue("\ngolemio import-db-data (idd)"));
            print.info(" Commands\n");
            print.info("  help (h)\t\t -");
            print.info("\n Flags\n");
            print.info("  --postgres\t\t Set database to PostgreSQL (default)");
            print.info("  --schema <name>\t Set database schema (postgres only)");
            print.info("  --dangerous\t\t Ignore danger error messages");
            return;
        }

        if (isPostgres) {
            if (!process.env.POSTGRES_CONN) {
                print.error("Environmental variable POSTGRES_CONN is not defined");
                process.exitCode = 1;
                return;
            }

            if (isDangerous) {
                print.warning("Dangerous mode is active");
            } else {
                const wordList = extractDangerousWords(process.env.POSTGRES_CONN);
                if (wordList.length > 0) {
                    print.error(`Environmental variable POSTGRES_CONN contains dangerous words: [${wordList.join(", ")}]`);
                    print.error("Suppress this error message with --dangerous");
                    process.exitCode = 1;
                    return;
                }
            }

            await importPostgresData(schema);
        }
    },
};

export default command;
