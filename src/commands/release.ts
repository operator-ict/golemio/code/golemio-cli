import fs from "fs";
import { GluegunCommand } from "gluegun";
import open from "open";
import path from "path";
import {
    VersionTagEnum,
    bumpVersionAndChangelog,
    check,
    checkMergedPipeline,
    checkModulesIntegrity,
    checkOpenMRPipeline,
    cloneRepo,
    commitChanges,
    createMR,
    createTagAndRelease,
    filterIds,
    getGitlabRepo,
    getVersions,
    printCommandHelp,
    printReleaseCandidateIssues,
    printReleaseCandidateRepos,
    ReleaseScopeEnum,
    removeRepoDir,
    syncBranches,
    upgradeGolemioDependencies,
} from "../utils/release.utils";

const TMP_DIR = path.resolve(process.cwd(), "tmp");

const command: GluegunCommand = {
    description: "Prepare new release",
    alias: "rls",
    run: async ({ parameters, print, prompt }) => {
        const [action] = parameters.array!;
        const { skip, only, interactive, scope } = parameters.options;

        if (!process.env.GITLAB_API_TOKEN) {
            print.error("Environmental variable GITLAB_API_TOKEN is not defined");
            process.exitCode = 1;
            return;
        }

        if (skip && only) {
            print.warning("Option --skip was skipped. Only one of --only or --skip can be used.");
        }

        if (
            (!scope || !Object.values(ReleaseScopeEnum).includes(scope)) &&
            ["check", "merge", "check-mr-pipelines", "sync-branches", "create-tags"].includes(action)
        ) {
            print.error(`Option --scope is not defined or does not match ${Object.values(ReleaseScopeEnum).join(", ")}`);
            process.exitCode = 1;
            return;
        }

        let repos: string[] = [];
        let releaseCandidates: string[] = [];
        let released: string[] = [];
        const releaseBranchName = process.env.RELEASE_BRANCH_NAME || "release";
        const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";

        switch (action) {
            case "help":
            case "h":
            default:
                printCommandHelp();
                break;

            case "check-modules-integrity":
                const missing = await checkModulesIntegrity();
                if (missing.length > 0) {
                    for (const miss of missing) {
                        print.warning(`Repository ${miss.name} (${miss.id}) missing in Core or module ids`);
                    }
                } else {
                    print.success("Core and module ids are up-to-date");
                }
                break;

            case "print-release-issue":
                await printReleaseCandidateIssues();

                let releaseCandidatesCore: string[] = [];
                let releaseCandidatesModules: string[] = [];
                // TODO services
                try {
                    const releaseCandidatesFileCore = fs.readFileSync(path.join(TMP_DIR, `release-candidates_core.json`));
                    releaseCandidatesCore = JSON.parse(releaseCandidatesFileCore.toString());
                    const releaseCandidatesFileModules = fs.readFileSync(path.join(TMP_DIR, `release-candidates_modules.json`));
                    releaseCandidatesModules = JSON.parse(releaseCandidatesFileModules.toString());
                } catch (err) {
                    print.warning(`Release candidates file(s) not found.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                }

                await printReleaseCandidateRepos("Core", releaseCandidatesCore);
                await printReleaseCandidateRepos("Modules", releaseCandidatesModules);
                // TODO services
                break;

            case "check":
                repos = filterIds(scope, { skip, only });

                for (const repoId of repos) {
                    const repo = await getGitlabRepo(repoId);
                    print.highlight(` ➔ ${repo.name} (${repo.id})`);
                    if (interactive) {
                        if (!(await prompt.confirm("Next?", true))) {
                            continue;
                        }
                    }

                    const clonedRepo = await cloneRepo(repo);

                    const checked = await check({ ...clonedRepo, url: repo.web_url });
                    if (checked.someDiffs) {
                        print.info(print.colors.yellow(` ‼ Anything changed`));
                        print.info(
                            JSON.stringify({ ...checked.diff, files: checked.diff?.files.map((f) => f.file).join(", ") }, null, 2)
                        );
                        print.info(print.colors.yellow(` ★ Changelog`));
                        print.info(checked.changelog);
                        print.info(print.colors.yellow(` ★ Link to graph`));
                        print.info(`${checked.webUrl}/-/network/${releaseBranchName}`);

                        // ask if repo is ready to release
                        if (await prompt.confirm("Is this repository release candidate?", true)) {
                            // save repo id for later
                            releaseCandidates.push(repoId);
                        }
                    } else if (checked.errorMessage) {
                        print.info(print.colors.bgRed(` ‼ Something went wrong`));
                        print.info(print.colors.muted(` \t${checked.errorMessage}`));
                    } else {
                        print.info(print.colors.muted(` \tNo diffs`));
                        // ask to force add repo to release candidates
                        if (scope === ReleaseScopeEnum.SERVICES) {
                            if (
                                await prompt.confirm(
                                    "Despite of no diffs do you want set this repository as release candidate?",
                                    true
                                )
                            ) {
                                // save repo id for later
                                releaseCandidates.push(repoId);
                            }
                        }
                    }

                    // delete repo directory
                    removeRepoDir(clonedRepo.dir);
                }

                // write release candidate repo ids to file
                fs.writeFileSync(path.join(TMP_DIR, `release-candidates_${scope}.json`), JSON.stringify(releaseCandidates));
                break;

            case "merge":
                try {
                    const releaseCandidatesFile = fs.readFileSync(path.join(TMP_DIR, `release-candidates_${scope}.json`));
                    releaseCandidates = JSON.parse(releaseCandidatesFile.toString());
                } catch (err) {
                    print.warning(`Release candidates file not found.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                }
                if (releaseCandidates.length === 0) {
                    print.warning(`Release candidates file is empty.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                    repos = filterIds(scope, { skip, only });
                } else {
                    repos = releaseCandidates;
                }

                for (const repoId of repos) {
                    const repo = await getGitlabRepo(repoId);
                    print.highlight(` ➔ ${repo.name} (${repo.id})`);
                    if (interactive) {
                        if (!(await prompt.confirm("Next?", true))) {
                            continue;
                        }
                    }

                    const { id, name, url, dir, git } = await cloneRepo(repo);

                    // show versions in branches and write correct version to package.json
                    const versions = await getVersions(dir, url, git);
                    print.info(` ★ Target branch version (${targetBranchName}): ${versions.target}`);
                    print.info(` ★ Current branch version (${releaseBranchName}): ${versions.current}`);
                    const { version } =
                        (await prompt.ask([
                            {
                                type: "input",
                                name: "version",
                                message: "Specify the version",
                            },
                        ])) || versions.current;
                    await bumpVersionAndChangelog(version, dir, git);

                    // upgrade @golemio dependencies
                    if (await prompt.confirm("Do you want upgrade @golemio dependencies?", true)) {
                        print.info(`✔ Upgrade @golemio dependencies to latest version`);
                        await upgradeGolemioDependencies(dir, VersionTagEnum.LATEST);
                    }

                    // commit package.json and CHANGELOG.md
                    await commitChanges(git);

                    // create new MR
                    const mr = await createMR(id, name);
                    print.success(` ★ Merge request was created (${mr.title}) ${mr.web_url}`);
                    await open(mr.web_url);

                    // save repo id for later
                    released.push(repoId);

                    // delete repo directory
                    removeRepoDir(dir);
                }

                // write released repo ids to file
                fs.writeFileSync(path.join(TMP_DIR, `released_${scope}.json`), JSON.stringify(released));
                break;

            case "check-mr-pipelines":
                try {
                    const releasedFile = fs.readFileSync(path.join(TMP_DIR, `released_${scope}.json`));
                    released = JSON.parse(releasedFile.toString());
                } catch (err) {
                    print.warning(`Released repos file not found.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                }
                if (released.length === 0) {
                    print.warning(`Released repos file is empty.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                    repos = filterIds(scope, { skip, only });
                } else {
                    repos = released;
                }

                for (const repoId of repos) {
                    const repo = await getGitlabRepo(repoId);
                    print.highlight(` ➔ ${repo.name} (${repo.id})`);
                    if (interactive) {
                        if (!(await prompt.confirm("Next?", true))) {
                            continue;
                        }
                    }

                    const openMRPipelines = await checkOpenMRPipeline(repo.id);
                    if (openMRPipelines && openMRPipelines?.length > 0) {
                        // MR is not merged yet, show MR pipelines
                        for (const pipeline of openMRPipelines) {
                            pipeline.status === "success"
                                ? print.success(`Merge request pipeline ${pipeline.id} passed\n${pipeline.web_url}`)
                                : print.warning(
                                      `Pipeline still running or failed. Status: ${pipeline.status}\n${pipeline.web_url}`
                                  );
                        }
                    } else {
                        // MR was merged, show last pipeline for target branch
                        print.info("No open Merge Requests");
                        const mergedPipeline = await checkMergedPipeline(repo.id);
                        mergedPipeline?.status === "success"
                            ? print.success(
                                  `Pipeline to ${process.env.TARGET_BRANCH_NAME} ${mergedPipeline.id} passed` +
                                      `\n${mergedPipeline.web_url}`
                              )
                            : print.warning(
                                  `Pipeline to ${process.env.TARGET_BRANCH_NAME} still running or failed. ` +
                                      `Status: ${mergedPipeline?.status}\n${mergedPipeline?.web_url}`
                              );
                    }
                }
                break;

            case "sync-branches":
                try {
                    const releasedFile = fs.readFileSync(path.join(TMP_DIR, `released_${scope}.json`));
                    released = JSON.parse(releasedFile.toString());
                } catch (err) {
                    print.warning(`Released repos file not found.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                }
                if (released.length === 0) {
                    print.warning(`Released repos file is empty.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                    repos = filterIds(scope, { skip, only });
                } else {
                    repos = released;
                }

                for (const repoId of repos) {
                    const repo = await getGitlabRepo(repoId);
                    print.highlight(` ➔ ${repo.name} (${repo.id})`);
                    if (interactive) {
                        if (!(await prompt.confirm("Next?", true))) {
                            continue;
                        }
                    }

                    const { dir, git } = await cloneRepo(repo);
                    // after merge changes to target branch, merge changes back to release and development branches
                    await syncBranches(repo, dir, git, true);
                    removeRepoDir(dir);
                }
                break;

            case "create-tags":
                try {
                    const releasedFile = fs.readFileSync(path.join(TMP_DIR, `released_${scope}.json`));
                    released = JSON.parse(releasedFile.toString());
                } catch (err) {
                    print.warning(`Released repos file not found.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                }
                if (released.length === 0) {
                    print.warning(`Released repos file is empty.`);
                    if (!(await prompt.confirm("Do you want continue?", false))) {
                        return;
                    }
                    repos = filterIds(scope, { skip, only });
                } else {
                    repos = released;
                }

                for (const repoId of repos) {
                    const repo = await getGitlabRepo(repoId);
                    print.highlight(` ➔ ${repo.name} (${repo.id})`);
                    if (interactive) {
                        if (!(await prompt.confirm("Next?", true))) {
                            continue;
                        }
                    }

                    const { dir, url, git } = await cloneRepo(repo);
                    // create new tag and release from target branch
                    await createTagAndRelease(repo.id, dir, url, git);
                    removeRepoDir(dir);
                }
                break;
        }
    },
};

export default command;
