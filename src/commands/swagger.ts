import { GluegunCommand } from "gluegun";
import { generateOpenApiConfig, printCommandHelp, runPortmanTests } from "../utils/swagger.utils";

const command: GluegunCommand = {
    description: "Generate OAS config or run Portman tests",
    alias: "oas",
    run: async ({ parameters, print }) => {
        const [action] = parameters.array!;
        let {
            header: oasHeaderFilePath,
            output: oasMergedOutputFilePath,
            oas: oasFilePath,
            filter: oasFilterFilePath,
            script: scriptFilePath,
            config: portmanConfigPath,
        } = parameters.options;

        const swaggerConfig = {
            oasHeaderFilePath: oasHeaderFilePath ?? null,
            oasFilesGlob: oasFilePath ?? null,
            oasFilePath: oasFilePath ?? null,
            oasMergedOutputFilePath: oasMergedOutputFilePath ?? null,
            oasFilterFilePath: oasFilterFilePath ?? null,
            scriptFilePath: scriptFilePath ?? null,
            portmanConfigPath: portmanConfigPath ?? null,
        };

        switch (action) {
            case "help":
            case "h":
            default:
                printCommandHelp();
                return;

            case "generate": {
                if (!swaggerConfig.oasMergedOutputFilePath || !swaggerConfig.oasFilesGlob) {
                    print.error("Missing required parameters");
                    process.exitCode = 1;
                    return;
                }

                const { isError, message } = generateOpenApiConfig(
                    swaggerConfig.oasHeaderFilePath,
                    swaggerConfig.oasFilesGlob,
                    swaggerConfig.oasFilterFilePath,
                    swaggerConfig.oasMergedOutputFilePath
                );

                if (isError) {
                    print.error(message);
                    process.exitCode = 1;
                } else {
                    print.success(message);
                }

                break;
            }

            case "api-test":
            case "test": {
                if (!swaggerConfig.oasFilePath || !swaggerConfig.scriptFilePath) {
                    print.error("Missing required parameters");
                    process.exitCode = 1;
                    return;
                }

                const { isError, message } = await runPortmanTests(
                    swaggerConfig.oasFilePath,
                    swaggerConfig.oasFilterFilePath,
                    swaggerConfig.scriptFilePath,
                    swaggerConfig.portmanConfigPath
                );

                if (isError) {
                    print.error(message);
                    process.exitCode = 1;
                } else {
                    print.success(message);
                }

                // Workaround for Portman test runner not exiting properly for some reason
                process.exit();
            }
        }
    },
};

export default command;
