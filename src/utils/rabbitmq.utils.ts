import amqplib from "amqplib";
import fs from "fs";
import { print } from "gluegun";

interface IRabbitMQSendArgs {
    exchangeName: string;
    queueKey: string;
    messageOrPathToFile: string;
}

interface IRabbitMQReceiveArgs {
    queueName: string;
    dequeue?: boolean;
}

export const sendData = async ({ exchangeName, queueKey, messageOrPathToFile }: IRabbitMQSendArgs) => {
    let drained = false;
    const conn = await amqplib.connect(process.env.RABBIT_CONN ?? " ");
    let channel = await conn.createChannel();
    channel.on("drain", () => {
        drained = true;
    });
    print.info("Connected to Queue!");
    conn.on("close", () => {
        print.info("Queue disconnected");
    });

    // from file if exists
    let buffer: Buffer;

    try {
        const stats = fs.statSync(messageOrPathToFile);
        if (!stats.isDirectory()) {
            buffer = fs.readFileSync(messageOrPathToFile);
        } else {
            throw new Error("Is dir");
        }
    } catch (err) {
        print.warning("File not found. Input sent as string.");
        buffer = Buffer.from(messageOrPathToFile);
    }

    // send message
    let ok = channel.publish(exchangeName, queueKey, buffer, {
        persistent: true,
    });
    if (!ok) {
        while (!drained) {
            await new Promise<void>((resolve) => setTimeout(() => resolve(), 10));
        }
    }
    print.highlight(`Message sent! Key: "${queueKey}", messageOrFile: "${messageOrPathToFile}"`);
    await new Promise<void>((resolve) =>
        setTimeout(() => {
            conn.close();
            resolve();
        }, 500)
    );
};

export const receiveData = async ({ queueName, dequeue }: IRabbitMQReceiveArgs) => {
    const conn = await amqplib.connect(process.env.RABBIT_CONN ?? " ");
    let channel = await conn.createChannel();
    print.info("Connected to Queue!");
    conn.on("close", () => {
        print.info("Queue disconnected");
    });

    const opts = { noAck: false };
    if (dequeue) {
        opts.noAck = true;
    }

    const msg = await channel.get(queueName, opts);
    if (msg) {
        print.highlight(`Headers:\n ${msg.properties ? JSON.stringify(msg.properties.headers) : "undefined"}`);
        print.highlight(`Content:\n ${msg.content.toString()}`);
        if (dequeue) {
            print.success("Message was dequeued.");
        }
    } else {
        print.warning("Queue is empty.");
    }

    await new Promise<void>((resolve) =>
        setTimeout(() => {
            conn.close();
            resolve();
        }, 500)
    );
};

/**
 * Print command help for rabbitmq
 */
export const printCommandHelp = () => {
    print.info(print.colors.blue("\ngolemio rabbitmq (rmq)"));
    print.info(" Commands\n");
    print.info("  send (s)\t\t Send message to exchange");
    print.info("  receive (r)\t\t Receive message from queue");
    print.info("  help (h)\t\t -");
    print.info("\n Flags\n");
    print.info("  --help\t\t Show help for specific command (send, receive)");
};

/**
 * Print command help for rabbitmq (send command)
 */
export const printCommandHelpSend = () => {
    print.info(print.colors.blue("\ngolemio rabbitmq (rmq) send (s)"));
    print.info("Flags\n");
    print.info("  --exchange <name>\t\t\t Set exchange name");
    print.info("  --queueKey <key>\t\t\t Set queue key to send to");
    print.info("  --messageOrPathToFile <messageOrFile>\t Set message to send or path to file");
};

/**
 * Print command help for rabbitmq (receive command)
 */
export const printCommandHelpReceive = () => {
    print.info(print.colors.blue("\ngolemio rabbitmq (rmq) receive (r)"));
    print.info("Flags\n");
    print.info("  --queueName <name>\t\t Set queue name to receive from");
    print.info("  --dequeue <true|false>\t Optional, if true, the message will be assumed by the server to be acknowledged");
};
