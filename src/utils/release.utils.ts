import { IssueSchema } from "@gitbeaker/core/dist/types/resources/Issues";
import { MergeRequestSchema } from "@gitbeaker/core/dist/types/resources/MergeRequests";
import { PipelineSchema } from "@gitbeaker/core/dist/types/resources/Pipelines";
import { ProjectExtendedSchema } from "@gitbeaker/core/dist/types/resources/Projects";
import { Gitlab } from "@gitbeaker/node";
import changelogParser from "changelog-parser";
import execa from "execa";
import fs from "fs";
import { print, prompt } from "gluegun";
import open from "open";
import path from "path";
import { DiffResult, SimpleGit, simpleGit } from "simple-git";
import coreIds from "../constants/release-core-ids.constant";
import moduleIds from "../constants/release-module-ids.constant";
import serviceIds from "../constants/release-service-ids.constant";
import dataprahaIds from "../constants/release-datapraha-ids.constant";
import golemiobiIds from "../constants/release-golemiobi-ids.constant";
import golemioczIds from "../constants/release-golemiocz-ids.constant";
import lkodIds from "../constants/release-lkod-ids.constant";
import vymiIds from "../constants/release-vymi-ids.constant";

const TMP_DIR = path.resolve(process.cwd(), "tmp");

export interface ICheckedRepo {
    id: number;
    name: string;
    someDiffs: boolean;
    diff?: DiffResult;
    changelog?: string;
    webUrl: string;
    errorMessage?: string;
}

interface IReleaseCheckArgs {
    skip?: string | string[];
    only?: string | string[];
}

interface IClonedRepoAttributes {
    id: number;
    name: string;
    url: string;
    dir: string;
    git: SimpleGit;
}

export enum VersionTagEnum {
    LATEST = "latest",
    DEV = "dev",
    RC = "rc",
}

export enum ReleaseScopeEnum {
    CORE = "core",
    MODULES = "modules",
    SERVICES = "services",
    WEB_DATAPRAHA = "web-datapraha",
    WEB_GOLEMIOBI = "web-golemiobi",
    WEB_GOLEMIOCZ = "web-golemiocz",
    WEB_LKOD = "web-lkod",
    WEB_VYMI = "web-vymi",
}

/**
 * Get data about repo from GitLab
 */
export const getGitlabRepo = async (repoId: string): Promise<ProjectExtendedSchema> => {
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    const repo = await gitlab.Projects.show(repoId);
    return repo;
};

/**
 * Clone repository
 */
export const cloneRepo = async (repo: ProjectExtendedSchema): Promise<IClonedRepoAttributes> => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";

    const id = repo.id;
    const name = repo.name;
    const url = repo.web_url.replace("https://", `https://gitlab-ci-token:${process.env.GITLAB_API_TOKEN}@`);
    const dir = fs.mkdtempSync(path.join(TMP_DIR, `repo-${name}-`));
    const git = simpleGit(dir);

    // clone repo and pull target branch (master)
    await git.clone(url, ".", { "--branch": branchName });

    return {
        id,
        name,
        url,
        dir,
        git,
    };
};

/**
 * Remove tmp directory with repository code
 */
export const removeRepoDir = (dir: string): void => {
    fs.rmSync(dir, { recursive: true, force: true });
};

/**
 * Check GitLab modules group
 * and returns repos which are missing in constants
 *
 * group id: 10431325
 * group url: https://gitlab.com/operator-ict/golemio/code/modules
 */
export const checkModulesIntegrity = async (): Promise<Array<{ id: string; name: string }>> => {
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    const allModulesInGroup = (await gitlab.Groups.projects("10431325"))
        .filter((proj) => !proj.archived)
        .sort((a, b) => a.name.localeCompare(b.name));

    const missing: Array<{ id: string; name: string }> = [];

    for (const m of allModulesInGroup) {
        if (!coreIds.includes(`${m.id}`) && !moduleIds.includes(`${m.id}`)) {
            missing.push({ id: `${m.id}`, name: m.name });
        }
    }
    return missing;
};

/**
 * Check repository if has any changes
 */
export const check = async (repo: IClonedRepoAttributes): Promise<ICheckedRepo> => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";

    let result: ICheckedRepo;

    try {
        // checkout to release branch and diff with master
        await repo.git.checkout(`origin/${branchName}`);
        await repo.git.pull("origin", branchName);
        const diff = await repo.git.diffSummary(`origin/${targetBranchName}`);

        // some changes are ready to release
        if (diff.changed > 0 || diff.deletions > 0 || diff.insertions > 0 || diff.files.length > 0) {
            const changelog = await getChangelogMessages(repo.dir);
            const unreleasedMessage = typeof changelog === "string" ? changelog : changelog.unreleased;

            result = {
                id: repo.id,
                name: repo.name,
                webUrl: repo.url,
                someDiffs: true,
                diff,
                changelog: unreleasedMessage,
            };
        } else {
            result = {
                id: repo.id,
                name: repo.name,
                webUrl: repo.url,
                someDiffs: false,
            };
        }
    } catch (err) {
        result = {
            id: repo.id,
            name: repo.name,
            webUrl: repo.url,
            someDiffs: false,
            errorMessage: err.message,
        };
    }

    return result;
};

/**
 * Return project versions in target and current (release) branch
 */
export const getVersions = async (dir: string, url: string, git: SimpleGit): Promise<{ target: string; current: string }> => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";

    // clone repo and pull target branch (master)
    await git.checkout(`origin/${targetBranchName}`);
    const targetVersion = JSON.parse(fs.readFileSync(`${dir}/package.json`).toString("utf-8")).version;

    // checkout to release branch and diff with master
    await git.checkout(`origin/${branchName}`);
    await git.pull("origin", branchName);
    const currentVersion = JSON.parse(fs.readFileSync(`${dir}/package.json`).toString("utf-8")).version;

    return {
        target: targetVersion,
        current: currentVersion,
    };
};

/**
 * Write version to package.json
 */
export const bumpVersionAndChangelog = async (version: string, dir: string, git: SimpleGit) => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    await git.checkout(`origin/${branchName}`);

    // version bump
    const packagejson = JSON.parse(fs.readFileSync(`${dir}/package.json`).toString("utf-8"));
    fs.writeFileSync(`${dir}/package.json`, JSON.stringify({ ...packagejson, version }, null, 4));

    // changelog bump
    try {
        if (!fs.existsSync(`${dir}/CHANGELOG.md`)) {
            throw new Error("CHANGELOG.md was not found.");
        }
        const parsedChangelog = await changelogParser(`${dir}/CHANGELOG.md`);
        const unreleased = {
            ...parsedChangelog.versions[0],
            version,
            title: `[${version}] - ${new Date().toISOString().split("T")[0]}`,
            date: new Date().toISOString().split("T")[0],
            // add no changelog message if there is no changelog (service specific)
            ...(!parsedChangelog.versions[0].body ? { body: "-   No changelog", parsed: { _: ["No changelog"] } } : {}),
        };
        parsedChangelog.versions.shift();
        const newChangelog = {
            ...parsedChangelog,
            versions: [
                {
                    version: null,
                    title: "[Unreleased]",
                    date: null,
                    body: "",
                    parsed: {
                        _: [],
                    },
                },
                unreleased,
                ...parsedChangelog.versions,
            ],
        };
        fs.writeFileSync(`${dir}/CHANGELOG.md`, stringifyChangelog(newChangelog));
    } catch (err) {
        print.warning("CHANGELOG.md was not found.");
    }
};

/**
 * Commit the changes to current (release) branch
 *
 * Commit message is simple (chore: ...)
 */
export const commitChanges = async (git: SimpleGit) => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";

    await git.checkout(`origin/${branchName}`);
    await git.pull("origin", branchName);
    await git.add(".");
    await git.commit("chore: bump version and update changelog");
    await git.push(["origin", `HEAD:refs/heads/${branchName}`]);
};

/**
 * Create merge request from current (release) to target (master) branch
 * @param id Repo id
 * @param name Repo name
 */
export const createMR = async (id: number, name: string): Promise<MergeRequestSchema> => {
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";
    const description = `This MR was created by Golemio CLI release command`;

    return gitlab.MergeRequests.create(id, branchName, targetBranchName, `[${name}] Golemio CLI Auto Release`, {
        description,
    });
};

/**
 * Return pipeline status of the open merge request
 * @param id Repo id
 */
export const checkOpenMRPipeline = async (
    id: number
): Promise<Array<Pick<PipelineSchema, "id" | "sha" | "ref" | "status" | "web_url">> | undefined> => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    const mrs = await gitlab.MergeRequests.all({ projectId: id, state: "opened", source_branch: branchName, sort: "desc" });
    if (mrs.length > 0) {
        const pipelines = await gitlab.MergeRequests.pipelines(id, mrs[0].iid);
        return pipelines.map((p) => ({ ...p, web_url: (p as any).web_url }));
    }
};

/**
 * Return status of the last five pipelines
 * @param id Repo id
 */
export const checkMergedPipeline = async (id: number): Promise<PipelineSchema | undefined> => {
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    const pipelines = await gitlab.Pipelines.all(id, { page: 1, perPage: 5 });
    const onlyToTarget = pipelines.filter((p) => p.ref === targetBranchName);
    if (onlyToTarget.length > 0) {
        return onlyToTarget[0];
    }
};

/**
 * Do a "back-merge"
 *
 * Fast-forward git pull of the target (master) to the current (release) branch
 * and git pull of the target (master) to the develoment branch. If no conflict was found than bump version and push
 */
export const syncBranches = async (
    repo: ProjectExtendedSchema,
    dir: string,
    git: SimpleGit,
    showChangelogBeforecommit: boolean = false
) => {
    const branchName = process.env.RELEASE_BRANCH_NAME || "release";
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";

    // current (release) branch sync
    print.info(`Synchronizing ${branchName} branch with ${targetBranchName} branch`);
    await git.checkout(`origin/${branchName}`);
    await git.pull("origin", branchName);
    await git.pull("origin", targetBranchName, ["--ff"]);
    await git.push(["origin", `HEAD:refs/heads/${branchName}`]);
    print.success(`${branchName} branch successfully synchronized`);

    // development branch sync (try)
    print.info(`Synchronizing development branch with ${targetBranchName} branch`);
    await git.checkout(`origin/development`);
    try {
        await git.pull("origin", targetBranchName, ["--ff"]);
        const status = await git.status();
        if (status.conflicted.length > 0) {
            throw new Error("Conflicts!");
        }
    } catch (err) {
        print.warning(
            `Merge target branch (${targetBranchName}) to development branch contains some conflicts or fails. ` +
                `Please sync development branch manually.\n${repo.web_url}`
        );
        return;
    }

    print.info("✔ Bump version in package.json");
    await execa("npm", ["version", "patch", "--no-git-tag-version"], { cwd: dir });
    print.info(`✔ Upgrade @golemio dependencies to dev version`);
    await upgradeGolemioDependencies(dir, VersionTagEnum.DEV);

    if (showChangelogBeforecommit) {
        print.info(`Please double check the changelog for merge inconsistencies and close the editor after.`);
        await open(`${dir}/CHANGELOG.md`, { wait: true });
        await prompt.confirm("After closing changelog press any key to continue...");
    }

    await git.add(".");
    await git.commit("chore: bump version");
    await git.push(["origin", `HEAD:development`]);
    print.success(`development branch successfully synchronized`);
};

/**
 * Create tag and release in repo. The last message from changelog is used as release note.
 *
 * @param id Repo id
 */
export const createTagAndRelease = async (id: number, dir: string, url: string, git: SimpleGit) => {
    const targetBranchName = process.env.TARGET_BRANCH_NAME || "master";
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });

    const version = (await getVersions(dir, url, git)).target;
    const changelog = await getChangelogMessages(dir);
    const tag = await gitlab.Tags.create(id, `v${version}`, targetBranchName);
    const release = await gitlab.Releases.create(id, {
        tag_name: tag.name,
        description: typeof changelog === "string" ? "" : changelog.lastVersion,
    });
    print.success(`Tag and Release name ${release.name} was created`);
};

/**
 * Print issues in Golemio group with state To Deploy
 */
export const printReleaseCandidateIssues = async () => {
    const gitlab = new Gitlab({ token: process.env.GITLAB_API_TOKEN });
    let issues: Array<Omit<IssueSchema, "epic">> = [];

    print.info(`**Related issues**\n`);
    for (let i = 1, imax = 5000 / 100; i <= imax; i++) {
        const issuesPartial = (
            await gitlab.Issues.all({
                groupId: 5666320, // https://gitlab.com/operator-ict/golemio
                page: i,
                perPage: 100, // max 100
            })
        ).filter(
            (issue) =>
                issue.state === "opened" &&
                (issue.labels as string[]).includes("team/vyvoj") &&
                (issue.labels as string[]).includes("state::To Deploy")
        );
        issues = [...issues, ...issuesPartial];
    }
    for (const issue of issues) {
        print.info(`- [${issue.title}](${issue.web_url})`);
    }
    print.info(``);
};

/**
 * Print table with release cantitdate repos
 */
export const printReleaseCandidateRepos = async (scope: string, ids: string[]) => {
    print.info(`**${scope}**\n`);
    print.info(`| Name  | Changelog  | Published  |`);
    print.info(`| --- | --- | --- |`);
    for (const id of ids) {
        const info = await getGitlabRepo(id);
        print.info(`| [${info.name}](${info.web_url})  | [Link](${info.web_url}/-/blob/development/CHANGELOG.md)  |  |`);
    }
    print.info(``);
};

/**
 * Filter repositories by args
 */
export const filterIds = (scope: string, { skip, only }: IReleaseCheckArgs): string[] => {
    let ids: string[] = [];
    switch (scope) {
        case ReleaseScopeEnum.CORE:
            ids = coreIds;
            break;
        case ReleaseScopeEnum.MODULES:
            ids = moduleIds;
            break;
        case ReleaseScopeEnum.SERVICES:
            ids = serviceIds;
            break;
        case ReleaseScopeEnum.WEB_DATAPRAHA:
            ids = dataprahaIds;
            break;
        case ReleaseScopeEnum.WEB_GOLEMIOBI:
            ids = golemiobiIds;
            break;
        case ReleaseScopeEnum.WEB_GOLEMIOCZ:
            ids = golemioczIds;
            break;
        case ReleaseScopeEnum.WEB_LKOD:
            ids = lkodIds;
            break;
        case ReleaseScopeEnum.WEB_VYMI:
            ids = vymiIds;
            break;
    }
    if (skip && !only) {
        return ids.filter((id) => (Array.isArray(skip) ? !`${skip}`?.includes(id) : `${skip}` !== id));
    }
    if (only) {
        return ids.filter((id) => (Array.isArray(only) ? `${only}`?.includes(id) : `${only}` === id));
    }
    return ids;
};

/**
 * Print command help for migrate-db
 */
export const printCommandHelp = () => {
    print.info(print.colors.blue("\ngolemio release (rls)"));
    print.info(" Commands\n");
    print.info("  check-modules-integrity\t Check module group if new repo was added");
    print.info("  print-release-issue\t\t Print issues and release candidate modules");
    print.info("  check\t\t\t\t Check repos that are ready to release");
    print.info("  merge\t\t\t\t Merge repos marked as release candidate by `check` command");
    print.info("  check-mr-pipelines\t\t Show pipelines of merged repos");
    print.info("  sync-branches\t\t\t Synchronize branches after release");
    print.info("  create-tags\t\t\t Create tags and releases of merged repos");
    print.info("  help (h)\t\t\t -\n");
    print.info(" Flags\n");
    print.info(`  --scope <${Object.values(ReleaseScopeEnum).join("|")}>\t The scope of repos`);
    print.info("  --skip <id>\t\t\t\t Repo id to be skipped (zero or many)");
    print.info("  --only <id>\t\t\t\t Repo id to be prefered (zero or many)");
    print.info("  --interactive\t\t\t\t Script waits for some action between repos");
};

/**
 * Get changelog messages
 */
const getChangelogMessages = async (dir: string): Promise<{ unreleased: string; lastVersion: string } | string> => {
    try {
        if (!fs.existsSync(`${dir}/CHANGELOG.md`)) {
            return "CHANGELOG.md was not found.";
        }
        const changelog = await changelogParser({ filePath: `${dir}/CHANGELOG.md` });
        return { unreleased: changelog.versions[0].body, lastVersion: changelog.versions[1].body };
    } catch (err) {
        return "CHANGELOG.md was not found.";
    }
};

/**
 * Stringify pased changelog
 */
const stringifyChangelog = (changelog: changelogParser.Changelog): string => {
    let changelogString = `# ${changelog.title}\n\n${changelog.description}\n\n`;
    for (const version of changelog.versions) {
        changelogString += `## ${version.title}\n\n${version.body ? `${version.body}\n\n` : ""}`;
    }
    return changelogString;
};

/**
 * Upgrade @golemio dependencies to latest version by versionTag
 */
export const upgradeGolemioDependencies = async (dir: string, versionTag: VersionTagEnum) => {
    const npmLsParams = ["ls", "-p", "--depth=0", "--package-lock-only"];
    if (versionTag !== VersionTagEnum.LATEST) {
        npmLsParams.push("--omit=dev");
    }
    const { stdout: dependencyPaths } = await execa("npm", npmLsParams, {
        cwd: dir,
    });

    const dependencies = dependencyPaths
        .match(/(@golemio(\\|\/).+)/gm)
        ?.map((dependency) => `${dependency}@${versionTag}`.replace("\\", "/"));

    const { stdout, stderr } = await execa("npm", ["install", "--ignore-scripts", "--save-exact"].concat(dependencies ?? []), {
        cwd: dir,
    });

    if (stderr) {
        print.error(stderr);
    } else {
        print.muted(stdout);
    }
};
