import type { PortmanOptions } from "@apideck/portman/dist/types/PortmanOptions";
import { Swagger } from "atlassian-openapi";
import glob from "fast-glob";
import fs from "fs";
import { print } from "gluegun";
import yaml from "js-yaml";
import { MergeInput, isErrorResult, merge } from "openapi-merge";
import { SingleMergeInput } from "openapi-merge/dist/data";
import path from "path";

interface IHelperFunctionResult {
    isError: boolean;
    message: string;
}

export const generateOpenApiConfig = (
    oasHeaderFilePath: string | null,
    oasFilesGlob: string,
    oasTagFilterFilePath: string | null,
    oasMergedOutputFilePath: string
): IHelperFunctionResult => {
    let mergeInput: MergeInput = [];
    if (oasHeaderFilePath) {
        mergeInput.push(loadOasYaml(oasHeaderFilePath, null));
    }

    for (const pathEntry of glob.sync(oasFilesGlob, { absolute: true })) {
        mergeInput.push(loadOasYaml(pathEntry, oasTagFilterFilePath));
    }

    const mergeResult = merge(mergeInput);
    if (isErrorResult(mergeResult)) {
        return { isError: true, message: `${mergeResult.message} (${mergeResult.type})` };
    }

    const outFilePath = path.resolve(process.cwd(), oasMergedOutputFilePath);
    fs.writeFileSync(outFilePath, JSON.stringify(mergeResult.output));

    return { isError: false, message: "Successfully generated OAS file at " + path.relative(process.cwd(), outFilePath) };
};

export const runPortmanTests = async (
    oasFilePath: string,
    filterFilePath: string | null,
    scriptFilePath: string,
    portmanConfigPath: string | null
): Promise<IHelperFunctionResult> => {
    let Portman: typeof import("@apideck/portman/dist/Portman")["Portman"];

    try {
        Portman = require("@apideck/portman/dist/Portman").Portman;
    } catch (err) {
        return { isError: true, message: "Failed to load Portman" };
    }

    const defaultPortmanConfigPath = path.resolve(__dirname, "..", "..", "static", "portman-config.json");
    const portmanConfigDir = fs.mkdtempSync("golemio-cli-portman");
    let portmanConfigFile: string | undefined;

    if (portmanConfigPath) {
        const defaultPortmanConfig = require(defaultPortmanConfigPath);
        const portmanConfig = require(path.resolve(process.cwd(), portmanConfigPath));
        const mergedConfig = { ...defaultPortmanConfig, ...portmanConfig };
        portmanConfigFile = path.resolve(portmanConfigDir, "portman-config.json");
        fs.writeFileSync(portmanConfigFile, JSON.stringify(mergedConfig));
    } else {
        portmanConfigFile = defaultPortmanConfigPath;
    }

    let filterFile: string | undefined;
    if (filterFilePath) {
        filterFile = path.resolve(process.cwd(), filterFilePath);
    } else {
        filterFile = path.resolve(__dirname, "..", "..", "static", "portman-filter.json");
    }

    const portmanOutputConfig: PortmanOptions = {
        baseUrl: "http://localhost:3011",
        oaLocal: path.resolve(process.cwd(), oasFilePath),
        oaOutput: path.resolve(process.cwd(), "docs/postman-output-collection-generated.json"),
        portmanConfigPath: portmanConfigFile,
        postmanConfigPath: path.resolve(__dirname, "..", "..", "static", "postman-config.json"),
        filterFile: filterFile,
        includeTests: true,
        runNewman: true,
        syncPostman: false,
        oaUrl: "",
        bundleContractTests: false,
        newmanIterationData: "",
        portmanConfigFile: "",
        postmanConfigFile: "",
        envFile: ".env",
    };

    try {
        const portman = new Portman(portmanOutputConfig);
        const { start, stop } = loadPortmanScript(path.resolve(process.cwd(), scriptFilePath));

        await start();
        await portman.run();
        await stop();

        return { isError: false, message: "Successfully ran Portman tests" };
    } catch (err) {
        return { isError: true, message: err.message };
    } finally {
        if (fs.existsSync(portmanConfigDir)) {
            fs.rmdirSync(portmanConfigDir, { recursive: true });
        }
    }
};

/**
 * Print command help for swagger
 */
export const printCommandHelp = () => {
    print.info(print.colors.blue("\ngolemio swagger (oas)"));
    print.info(" Commands\n");
    print.info("  generate \t\t Generate OAS file from multiple inputs");
    print.info("  api-test \t\t Run Portman integration tests");
    print.info("  help (h)\t\t -");
    print.info("\n Flags\n");
    print.info("  --oas <path_or_glob>\t OAS generator input file glob or Portman OAS file path");
    print.info("  --header <path>\t OAS generator header file path");
    print.info("  --output <path>\t OAS generator JSON output file path");
    print.info("  --filter <path>\t OAS generator tag filter file path or Portman filter file path");
    print.info("  --script <path>\t Portman run script file path (start/stop)");
    print.info("  --config <path>\t Portman config file path");
};

const loadOasYaml = (oasFilePath: string, tagFilterPath: string | null): SingleMergeInput => {
    const oasFileContents = fs.readFileSync(oasFilePath, "utf8");
    const tagFilterList = tagFilterPath ? require(path.resolve(process.cwd(), tagFilterPath)) : null;

    return {
        oas: yaml.load(oasFileContents) as Swagger.SwaggerV3,
        operationSelection: {
            excludeTags: Array.isArray(tagFilterList) ? tagFilterList : [],
        },
    };
};

export const loadPortmanScript = (filePath: string): { start: () => Promise<void>; stop: () => Promise<void> } => {
    const script = require(filePath);
    if (typeof script.start !== "function" || typeof script.stop !== "function") {
        throw new Error("Invalid Portman script");
    }

    return { start: script.start, stop: script.stop };
};
