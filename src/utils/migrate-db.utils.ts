import { spawn } from "cross-spawn";
import dbm from "db-migrate";
import fs from "fs";
import { print } from "gluegun";
import { readFile } from "node:fs/promises";
import path from "path";
import { parse as parsePostgresConn } from "pg-connection-string";

const DEFAULT_MODULE_PG_SCHEMA_PLACEHOLDER = "$DEFAULT_MODULE_PG_SCHEMA";
const SUPPORTED_LOCKFILE_VERSIONS = [3];

export interface IMigrateConfig {
    schema: string;
}

/** Not a complete lockfile type, only the properties needed are included */
type Lockfile = {
    lockfileVersion: number;
    packages?: Record<
        string,
        {
            dependencies?: Record<string, string>;
            devDependencies?: Record<string, string>;
            peerDependencies?: Record<string, string>;
            optionalDependencies?: Record<string, string>;
            [key: string]: unknown;
        }
    >;
    [key: string]: unknown;
};

export interface IPostgresMigrateArgs {
    connectionString: string;
    migrationDir: string;
    defaultModulePgSchemas?: Map<string, string>;
    defaultSchema?: string;
    includeLocalConfig?: boolean;
    lockfileData?: Lockfile;
}

export type CommandResult = [string, string, boolean, string?];

/**
 * Run command shim (node_modules/.bin/)
 */
export const runCommandShim = (binaryName: "yarn" | "npm", command: string): Promise<void> => {
    const chProcess = spawn(binaryName, [binaryName === "yarn" ? "run" : "exec", ...command.split(" ")], { stdio: "inherit" });

    return new Promise((resolve, reject) => {
        chProcess.on("error", reject);
        chProcess.on("exit", (code) => (code === 0 ? resolve() : reject("Command exited with non-zero code")));
    });
};

export async function getDefaultModulePgSchemas(
    migrationDirs: Array<IPostgresMigrateArgs["migrationDir"]>
): Promise<Map<string, string>> {
    const defaultModulePgSchemas = new Map<string, string>();
    const promises = migrationDirs.map(getDefaultModulePgSchema);
    const results = await Promise.all(promises);
    for (const res of results) {
        if (res === null) continue;
        defaultModulePgSchemas.set(res.modulePath, res.schema);
    }
    return defaultModulePgSchemas;
}

async function getDefaultModulePgSchema(
    migrationDir: IPostgresMigrateArgs["migrationDir"]
): Promise<{ modulePath: string; schema: string } | null> {
    const migrationConfigLocation = path.resolve(migrationDir, ".config.json");
    if (!fs.existsSync(migrationConfigLocation)) {
        print.warning(`Local config ${migrationConfigLocation} not found.`);
        return null;
    }
    const dbmConfig = JSON.parse(await readFile(migrationConfigLocation, "utf-8"));
    if (!dbmConfig.isSchemaModuleDefault) return null;
    const modulePath = getModulePathFromAnyPath(migrationDir);
    return { modulePath, schema: dbmConfig.schema };
}

function getModulePathFromAnyPath(path: string): string {
    const matches = [...path.matchAll(/.*?(node_modules\/(?:@.+?\/.+?|.+?))\//g)];
    if (matches.length === 0) return "";
    const lastMatch = matches.at(-1)!;
    if (typeof lastMatch.at(-1) !== "string") throw new Error("Invalid match");
    return lastMatch.at(1) as string;
}

export function readLockfile(): Lockfile {
    if (fs.existsSync("./package-lock.json")) {
        return JSON.parse(fs.readFileSync("./package-lock.json", "utf-8")) as Lockfile;
    }
    if (fs.existsSync("./npm-shrinkwrap.json")) {
        return JSON.parse(fs.readFileSync("./npm-shrinkwrap.json", "utf-8")) as Lockfile;
    }
    throw new Error("No lockfile data provided. Check if the service image contains valid package-lock");
}

/**
 * Create and return new db-migrate instances
 */
export function getPostgresMigrateInstances({
    connectionString,
    migrationDir,
    defaultModulePgSchemas,
    defaultSchema,
    includeLocalConfig,
    lockfileData,
}: IPostgresMigrateArgs) {
    // Default config
    let dbmConfig: IMigrateConfig = { schema: "public" };
    const migrationConfigLocation = path.resolve(migrationDir, ".config.json");

    if (includeLocalConfig) {
        if (fs.existsSync(migrationConfigLocation)) {
            dbmConfig = {
                ...dbmConfig,
                ...JSON.parse(fs.readFileSync(migrationConfigLocation).toString("utf8")),
            };
        } else {
            print.warning(
                `Local config ${migrationConfigLocation} not found. ` +
                    `Using the default configuration\n${JSON.stringify(dbmConfig, null, 4)}`
            );
        }
    }

    if (typeof defaultSchema === "string") {
        dbmConfig.schema = defaultSchema;
    }

    const schemas: string[] = [];
    if (dbmConfig.schema.includes(DEFAULT_MODULE_PG_SCHEMA_PLACEHOLDER) && lockfileData) {
        if (!defaultModulePgSchemas) {
            throw new Error("Default module schemas required for placeholder");
        }
        const modulePath = getModulePathFromAnyPath(migrationDir);
        const dependantsPaths = getModuleDependants(lockfileData, modulePath);
        for (const dependant of dependantsPaths) {
            if (!defaultModulePgSchemas.has(dependant)) continue;
            const schema = defaultModulePgSchemas.get(dependant)!;
            schemas.push(dbmConfig.schema.replace(DEFAULT_MODULE_PG_SCHEMA_PLACEHOLDER, schema));
        }
    } else {
        schemas.push(dbmConfig.schema);
    }

    if ("isSchemaModuleDefault" in dbmConfig) {
        const { isSchemaModuleDefault, ...rest } = dbmConfig;
        dbmConfig = rest;
    }
    const { user, password, host, database, port, ssl } = parsePostgresConn(connectionString);
    const migrations = schemas.map((schema) =>
        dbm.getInstance(true, {
            config: {
                postgres: {
                    ...dbmConfig,
                    schema,
                    driver: "pg",
                    user,
                    password,
                    host,
                    database,
                    port: port?.toString() ?? "5432",
                    ssl: !!ssl,
                },
            },
            cmdOptions: {
                "migrations-dir": migrationDir,
                "sql-file": true,
            },
            env: "postgres",
        })
    );

    return {
        migrations,
        schemas,
        createLocalConfig: () => {
            if (!fs.existsSync(migrationConfigLocation)) {
                fs.writeFileSync(migrationConfigLocation, JSON.stringify(dbmConfig, null, 4));
                print.info("[INFO] Created local config at " + migrationConfigLocation);
            }
        },
    };
}

function getModuleDependants(lockfileData: Lockfile, modulePath: string): string[] {
    if (!SUPPORTED_LOCKFILE_VERSIONS.includes(lockfileData.lockfileVersion)) {
        throw new Error(`Unsupported lockfile version "${lockfileData.lockfileVersion}"`);
    }
    if (!lockfileData.packages) return [];
    const moduleName = modulePath.replace("node_modules/", "");
    const dependantPkgs: string[] = [];
    for (const pkgPath of Object.keys(lockfileData.packages)) {
        const pkg = lockfileData.packages[pkgPath];
        const allDependencies = {
            ...(pkg.dependencies ?? {}),
            ...(pkg.devDependencies ?? {}),
            ...(pkg.peerDependencies ?? {}),
            ...(pkg.optionalDependencies ?? {}),
        };
        if (Object.keys(allDependencies).includes(moduleName)) {
            dependantPkgs.push(pkgPath);
        }
    }
    return dependantPkgs;
}

/**
 * Print command help for migrate-db
 */
export const printCommandHelp = () => {
    print.info(print.colors.blue("\ngolemio migrate-db (mdb)"));
    print.info(" Commands\n");
    print.info("  create <name>\t\t Create new migration");
    print.info("  up\t\t\t Execute up migrations");
    print.info("  down\t\t\t Execute down migrations");
    print.info("  reset\t\t\t Reset all executed migrations");
    print.info("  help (h)\t\t -");
    print.info("\n Flags\n");
    print.info("  --postgres\t\t Set database to PostgreSQL (default)");
    print.info("  --path\t\t Set path to migration director(y/ies) (default db/migrations/postgresql)");
    print.info("  --schema <name>\t Set database schema (postgres only)");
    print.info("  --silent\t\t Ignore database driver stdout (postgres only)");
};

/**
 * Transform command results and print summary matrix
 */
export const printSummaryMatrix = (results: CommandResult[]) => {
    const summaryMatrix: Array<[string, string, string, string]> = results.map(([dir, schema, isSuccess, info]) => {
        const migrationsPath = path.relative(process.cwd(), dir);
        const isModule = migrationsPath.startsWith("node_modules");

        return [
            isModule ? `Module: ${migrationsPath.match(/node_modules\/([^\/]*\/[^\/]*)/s)?.[1]}` : `Path: ${migrationsPath}`,
            schema.replaceAll(/(,| )+(public)/g, ""),
            isSuccess ? print.colors.green("Success") : print.colors.red("Fail"),
            info ?? "\uD83E\uDD73",
        ];
    });

    print.info(print.colors.blue("\nSummary"));
    print.table([["Location", "Schema", "Result", "Info"], ...summaryMatrix], {
        format: "lean",
        style: { "padding-left": 0, "padding-right": 8 },
    });
};
