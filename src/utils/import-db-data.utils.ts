import fs from "fs";
import path from "path";
import glob from "fast-glob";
import { Pool } from "pg";
import format from "pg-format";
import { print } from "gluegun";

const DANGEROUS_WORDS = ["rabin", "golem"];

/**
 * Import SQL dumps
 */
export const importPostgresData = async (defaultSchema?: string) => {
    const files = glob.sync(process.env.SQL_DUMP_FILES ?? " ");

    if (files.length < 1) {
        print.error(`No SQL data to import`);
        process.exitCode = 1;
        return;
    }

    const pool = new Pool({ connectionString: process.env.POSTGRES_CONN });
    const client = await pool.connect();

    let currentDirName = "";
    let originalSearchPaths: string[] | undefined = undefined;

    for (const file of files) {
        if (path.dirname(file) !== currentDirName) {
            print.divider();

            currentDirName = path.dirname(file);
            originalSearchPaths =
                originalSearchPaths || ((await client.query("SHOW search_path")).rows[0].search_path.split(",") as string[]);

            let searchPaths: string[];
            const schema: string | undefined =
                defaultSchema ||
                (() => {
                    const configLocation = path.resolve(currentDirName, ".config.json");
                    return fs.existsSync(configLocation) && JSON.parse(fs.readFileSync(configLocation).toString("utf8")).schema;
                })();

            if (schema && !originalSearchPaths.some((path) => new RegExp(`\\b${schema}\\b`, "i").test(path))) {
                searchPaths = [schema, ...originalSearchPaths];
                for (let path of searchPaths) {
                    if (path.indexOf('"') !== 0) {
                        path = `"${path.trim()}"`;
                    }
                }

                print.highlight(`➔ using schema '${schema}'`);
            } else {
                searchPaths = originalSearchPaths;
                print.highlight(`➔ using original schema`);
            }

            if (searchPaths.length > 0) {
                await client.query(format("SET search_path TO %s", searchPaths));
            }
        }

        print.info(`Restoring SQL data from '${file}'`);
        const query = fs.readFileSync(file).toString();

        try {
            await client.query(query);
        } catch (err) {
            if (err instanceof Error) {
                print.error(`${print.xmark} ${err.message} `);
            }

            process.exitCode = 1;
            break;
        }
    }

    client.release();
    pool.end();
};

export const extractDangerousWords = (str: string): RegExpMatchArray | string[] => {
    const regexp = new RegExp(DANGEROUS_WORDS.join("|"), "gi");
    return str.match(regexp) ?? [];
};
