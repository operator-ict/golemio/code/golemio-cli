import bundle from "@asyncapi/bundler";
import { ClientSecretCredential } from "@azure/identity";
import { BlobServiceClient, BlockBlobParallelUploadOptions } from "@azure/storage-blob";
import glob from "fast-glob";
import fs, { writeFileSync } from "fs";
import { print } from "gluegun";
import path from "path";

interface IHelperFunctionResult {
    isError: boolean;
    message: string;
}

export const generateAsyncApiConfig = async (
    oasHeaderFilePath: string | null,
    oasFilesGlob: string,
    oasMergedOutputFilePath: string
): Promise<IHelperFunctionResult> => {
    try {
        let mergeInput: string[] = [];

        for (const pathEntry of glob.sync(oasFilesGlob, { absolute: true })) {
            mergeInput.push(pathEntry);
        }

        //accepts last path in an array as header
        if (oasHeaderFilePath) {
            mergeInput.push(oasHeaderFilePath);
        }

        const document = await bundle(mergeInput, {
            xOrigin: true,
        });
        const outFilePath = path.resolve(process.cwd(), oasMergedOutputFilePath);
        if (document.yml()) {
            writeFileSync(outFilePath, document.yml() as any);
        }
        return {
            isError: false,
            message: "Successfully generated asyncapi file at " + path.relative(process.cwd(), outFilePath),
        };
    } catch (error) {
        return { isError: true, message: `${error.message}` };
    }
};

export const uploadFile = async (input: string, output: string): Promise<IHelperFunctionResult> => {
    try {
        const util = require("util");
        const exec = util.promisify(require("child_process").exec);
        const file = `${output}/index.html`;

        const cliCommand = createAsyncapiCliCommand(input, output);
        await exec(cliCommand);

        const credentials = new ClientSecretCredential(
            process.env.AZURE_BLOB_TENANT_ID!,
            process.env.AZURE_BLOB_CLIENT_ID!,
            process.env.AZURE_BLOB_CLIENT_SECRET!
        );
        const blobServiceClient = new BlobServiceClient(
            `https://${process.env.AZURE_BLOB_ACCOUNT!}.blob.core.windows.net`,
            credentials
        );
        const containerClient = blobServiceClient.getContainerClient(process.env.AZURE_BLOB_CONTAINER_NAME!);

        const blobClient = containerClient.getBlockBlobClient(file);

        const filePath = path.resolve(process.cwd(), file);

        const fileToBeUploaded = await fs.readFileSync(filePath);

        const options: BlockBlobParallelUploadOptions = {
            blobHTTPHeaders: { blobContentType: "text/html" },
        };

        await blobClient.uploadData(fileToBeUploaded, options);

        return { isError: false, message: `File ${file} uploaded.` };
    } catch (error) {
        return { isError: true, message: `${error.message}` };
    }
};

/**
 * Create command for asyncapi cli
 * @param input
 * @param output
 * @returns
 */
const createAsyncapiCliCommand = (input: string, output: string) => {
    // eslint-disable-next-line max-len
    return `asyncapi generate fromTemplate ${input} @asyncapi/html-template@2.3.14 --output ${output} -p singleFile=true --no-interactive --force-write`;
};

/**
 * Print command help for swagger
 */
export const printCommandHelp = () => {
    print.info(print.colors.blue("\ngolemio asyncapi (asp)"));
    print.info(" Commands\n");
    print.info("  generate \t\t Generate asyncapi documentation from multiple inputs");
    print.info("  upload \t\t Build and upload asyncapi documentation to Azure Blob Storage");
};
