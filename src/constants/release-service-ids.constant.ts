export default [
    "14373681", // integration-engine
    "14144379", // output-gateway
    "14144233", // input-gateway
    "25935253", // transport-input-gateway
    "48711503", // access-proxy
    "48711454", // permission-management
    "29145478", // url-proxy
    "29487154", // data-proxy
    "14116679", // cron-tasks
    "48860138", // dlq-consumer
    "29510928", // VP - Integration Engine
    "29633883", // VP - Input Gateway
    "29603657", // VP - Output Gateway
    "32789250", // 'Api Keys Management'
    "27966098", // 'Permission proxy - Client'
];
