export default [
    "13721357", // errors
    "23585902", // eslint-config
    "14030308", // validator
    "23519308", // core
    "30734762", // db-common
    "45275112", // traffic-common
    "37017335", // ovapi-gtfs-realtime-bindings
    "48711528", // permission-common
    "29993507", // golemio-cli
];
