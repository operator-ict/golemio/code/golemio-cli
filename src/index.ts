import { build } from "gluegun";

export const run = (argv: string[]) => {
    const runtime = build().brand("golemio").src(__dirname).help().defaultCommand().create();
    return runtime.run(argv);
};
