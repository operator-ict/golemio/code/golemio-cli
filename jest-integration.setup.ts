// @ts-ignore
import * as failFast from "jasmine-fail-fast";

// Fail fast jasmine hack for integration tests
// https://github.com/facebook/jest/issues/2867
const jasmineEnv = (jasmine as any).getEnv();
jasmineEnv.addReporter(failFast.init());
