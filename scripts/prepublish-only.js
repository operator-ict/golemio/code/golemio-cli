const fs = require("fs");
const path = require("path");
const pkg = require("../package.json");

delete pkg.scripts.postinstall;
fs.writeFileSync(path.resolve(process.cwd(), "package.json"), JSON.stringify(pkg, null, 4));
