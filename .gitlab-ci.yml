image: node:20.12.2-alpine

include:
    - project: "operator-ict/devops/gitlab-ci-pipeline"
      file:
          - '/golemio-globals/service-versions.yml'
          - "/docs/golemio-backstage-techdocs.yml"

stages:
    - unit_tests
    - integration_tests
    - publish
    - deploy

variables:
    POSTGRES_HOST: postgres
    POSTGRES_USER: oict
    POSTGRES_PASSWORD: oict-pass
    POSTGRES_DB: oict-test
    BACKSTAGE_ENTITY_NAME: "default/component/golemio-cli"

cache: &global_cache
    key:
        files:
            - package-lock.json
    paths:
        - node_modules
        - .eslintcache
    policy: pull

run_unit_tests:
    stage: unit_tests
    coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
    cache:
        <<: *global_cache
        policy: pull-push
    script:
        - npm install --ignore-scripts --progress=false
        # Audit production dependencies for CRITICAL vulnerabilities
        - npm audit --omit dev --audit-level critical
        - npm run lint
        - npm run build
        - npm run code-coverage
    artifacts:
        when: always
        reports:
            junit:
                - junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/cobertura-coverage.xml

    tags:
        - docker
    rules:
        - when: always

run_integration_tests:
    stage: integration_tests
    services:
        - !reference [.postgis-image]
    script:
        - export POSTGRES_CONN=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB
        - npm install --ignore-scripts --progress=false
        - npm run test-integration
    tags:
        - docker
    rules:
        - when: always

create_and_run_migrations:
    stage: integration_tests
    services:
        - !reference [.postgis-image]
    script:
        - export POSTGRES_CONN=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB
        - export POSTGRES_MIGRATIONS_DIR=.migrations/postgresql
        - npm install --ignore-scripts --progress=false
        - ./bin/golemio.js migrate-db create test --schema test,public
        - echo "CREATE TABLE test (point geometry NOT NULL); INSERT INTO test VALUES ('POINT(0 0)');" > $POSTGRES_MIGRATIONS_DIR/sqls/*-test-up.sql
        - echo "DROP TABLE test;" > $POSTGRES_MIGRATIONS_DIR/sqls/*-test-down.sql
        - ./bin/golemio.js migrate-db up --silent
        - ./bin/golemio.js migrate-db down --silent
    tags:
        - docker
    rules:
        - when: always

npm_publish:
    stage: publish
    script:
        - tmp=$(mktemp)
        - >
            if [ "$CI_COMMIT_REF_NAME" != "master" ]; then
                export PACKAGE_VERSION=$(node -p "require('./package.json').version")
                npm version --no-git-tag-version --new-version "$PACKAGE_VERSION-dev.$CI_PIPELINE_ID"
                cat package.json
            fi
        - echo '//registry.npmjs.org/:_authToken=${NPMJS_TOKEN}'>.npmrc
        - npm install --ignore-scripts --progress=false
        - npm run build
        - export NPM_TAG=latest
        - if [ "$CI_COMMIT_REF_NAME" != "master" ]; then export NPM_TAG=dev; fi
        - npm publish --access public --tag $NPM_TAG
    rules:
        - if: $CI_COMMIT_BRANCH == "master"
        - if: $CI_COMMIT_BRANCH == "development"
    tags:
        - docker
