#!/usr/bin/env node

const fs = require("fs");
const path = require("path");

const APP_ROOT_PATH = fs.realpathSync(process.cwd());
require("dotenv").config({ path: path.resolve(APP_ROOT_PATH, ".env") });

const sourcePath = path.resolve(__dirname, "..", "src");
let distPath = path.resolve(__dirname, "..", "dist");

// Use ts-node loader in development mode
if (process.env.NODE_ENV !== "test" && fs.existsSync(sourcePath)) {
    const register = require("ts-node").register;
    const tsconfigPath = path.resolve(__dirname, "..", "tsconfig.build.json");
    register({ project: tsconfigPath, compilerOptions: { noImplicitAny: false } });
    distPath = sourcePath;
}

require(path.resolve(distPath)).run(process.argv);
