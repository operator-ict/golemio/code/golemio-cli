module.exports = {
    globals: {
        "ts-jest": {
            tsconfig: "tsconfig.json",
        },
    },
    testRunner: "jest-jasmine2",
    setupFiles: ["dotenv/config"],
    setupFilesAfterEnv: ["./jest-integration.setup.ts"],
    moduleFileExtensions: ["js", "ts"],
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    testRegex: "./test/integration/.*.ts",
    testEnvironment: "node",
    testTimeout: 100000,
};
