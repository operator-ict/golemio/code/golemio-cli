# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.9.2] - 2025-02-13

### Changed

-   cleanup release constants

## [1.9.1] - 2025-02-13

### Added

-   audit logs release

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

### Removed

-   golemio BI permission services release

## [1.9.0] - 2025-01-23

### Changed

-   Better npm lockfile handling

## [1.8.0] - 2025-01-02

### Added

-   Support for default module database migration schemas and schema placeholders ([general#647](https://gitlab.com/operator-ict/golemio/code/general/-/issues/647))

## [1.7.3] - 2024-10-16

-   No changelog

## [1.7.2] - 2024-10-16

### Added

-   asyncAPI upload documentation to azure ([integration-engine#267](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/267))

## [1.7.1] - 2024-09-17

### Added

-   add golemio BI permission services constants ([gbi-general#44](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/44))

## [1.7.0] - 2024-09-12

### Added

-   `--path` support for `golemio migrate-db` command ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

### Changed

-   `golemio migrate-db` does not continue to run other migrations if one fails

## [1.6.3] - 2024-09-02

### Added

-   asyncAPI merge yaml files ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))

## [1.6.2] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.6.1] - 2024-08-08

### Added

-   Add web scopes to release commands

## [1.6.0] - 2024-08-08

### Added

-   Release command - add upgrading @golemio dependencies to dev tag while syncing branches
-   Release command - add force setting the repository as release candidate
-   ovapi-gtfs-realtime-bindings module
-   errors, validator, eslint-config module
-   dlq-consumer service

### Changed

-   Replace release of PP with release of Permission Services
-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

### Fixed

-   Release command - add `no changelog` message to services without any changes

## [1.5.0] - 2023-08-22

### Added

-   Swagger command - generate OAS config or run Portman tests ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.4.6] - 2023-08-08

### Changed

-   Release command - parse CHANGELOG to json and automatically bump version (no manual version bumping needed in CHANGELOG)

### Removed

-   Remove archived modules

## [1.4.5] - 2023-08-04

### Removed

-   Remove mongo commands

### Security

-   Update dependencies

### Changed

-   Update Node.js to v18.17.0

## [1.4.4] - 2023-07-10

### Added

-   vehiclesharing module

## [1.4.3] - 2023-06-14

### Added

-   traffic-common module

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.4.2] - 2023-02-26

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.4.1] - 2023-01-30

### Fixed

-   Bump version after syncing branches

## [1.4.0] - 2023-01-25

### Added

-   Add release commands ([docs](./docs/release_management_CZ.md))

### Changed

-   Migrate to npm

## [1.3.3] - 2022-10-20

### Added

-   Silent mode (parameter of mdb)

### Changed

-   Update db-migrate to v1.0.0-beta.18

## [1.3.2] - 2022-10-03

### Added

-   Dangerous mode (parameter of idd)

### Changed

-   Match postgres connection string against a list of dangerous words (idd only)
-   Make the postgres parameter optional (postgres is now the default)

## [1.3.1] - 2022-07-28

### Changed

-   Update db-migrate pg driver to [v1.2.3-diversion.4](https://gitlab.com/operator-ict/golemio/code/db-migrate-pg/-/releases/v1.2.3-diversion.4)

## [1.3.0] - 2022-07-12

### Added

-   Add rabbitmq commands (sending and receiving messages)

## [1.2.2] - 2022-05-02

### Changed

-   Stop printing "public" as a secondary schema in the summary matrix
-   Print migrations' location (module or path) in the summary matrix

## [1.2.1] - 2022-03-01

### Changed

-   Update db-migrate pg driver to [v1.2.3-diversion.3](https://gitlab.com/operator-ict/golemio/code/db-migrate-pg/-/releases/v1.2.3-diversion.3)

## [1.2.0] - 2021-11-12

### Added

-   import-test-data schema configuration

### Changed

-   migrate-db config location

## [1.1.1] - 2021-11-09

### Fixed

-   Check for Mongo dependencies

