import fs from "fs";
import path from "path";
import { fork, ForkOptions } from "child_process";
import crypto from "crypto";
import glob from "fast-glob";
import rimraf from "rimraf";
import { Pool, PoolClient } from "pg";

const forkProcess = async (executablePath: string, args: string[], options: ForkOptions): Promise<void> => {
    const chProcess = fork(executablePath, args, options);

    return new Promise((resolve, reject) => {
        chProcess.on("error", reject);
        chProcess.on("exit", (code) => (code === 0 ? resolve() : reject("Command exited with non-zero code")));
    });
};

const fillSqlUpScript = (executablePath: string, schema: string) => {
    const [, index] = /2.*-test-(.{1,})-up\.sql/.exec(path.basename(executablePath))!;
    fs.writeFileSync(
        executablePath,
        `
            CREATE TABLE "${schema}".test_${index} (
                hey int,
                cheers varchar(255)
            );
        `.trim()
    );
};

const fillSqlDownScript = (executablePath: string, schema: string) => {
    const [, index] = /2.*-test-(.{1,})-down\.sql/.exec(path.basename(executablePath))!;
    fs.writeFileSync(executablePath, `DROP TABLE "${schema}".test_${index};`);
};

const getTableCount = async (client: PoolClient, schema: string) => {
    const countResult = await client.query({
        text: `SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${schema}';`,
        rowMode: "array",
    });

    return +countResult.rows[0];
};

/**
 * Integration test for migrate-db
 */
describe("I&T migrate-db", () => {
    const ITERATIONS = process.env.MIGRATIONS_TEST_ITERATIONS ? +process.env.MIGRATIONS_TEST_ITERATIONS : 3;
    const executablePath = path.resolve(__dirname, "../..", "bin/golemio.js");

    // =============================================================================
    // PostgreSQL
    // =============================================================================
    describe("PostgreSQL", () => {
        const migrationsPath = path.resolve(__dirname, ".migrations", "postgresql");
        const schema = crypto.randomBytes(15).toString("hex");
        let pool: Pool;
        let client: PoolClient;

        const cleanUp = async () => {
            await client?.query(`DROP SCHEMA IF EXISTS "${schema}" CASCADE;`);
            client?.release();
            pool?.end();
        };

        beforeAll(() => {
            if (fs.existsSync(migrationsPath)) {
                rimraf.sync(migrationsPath);
            }
        });

        afterAll(() => cleanUp());

        test("generate PostgreSQL migrations", async () => {
            for (let i = 0; i < ITERATIONS; i++) {
                await forkProcess(executablePath, ["mdb", "create", "test-" + i, "--postgres"], {
                    stdio: "ignore",
                    env: {
                        PATH: process.env.PATH,
                        NODE_ENV: "test",
                        POSTGRES_CONN: process.env.POSTGRES_CONN,
                        POSTGRES_MIGRATIONS_DIR: migrationsPath,
                    },
                }).catch(() => cleanUp());
            }

            expect(glob.sync("*-test*.js", { cwd: migrationsPath })).toHaveLength(ITERATIONS);
        });

        test("run up migrations", async () => {
            const sqlUpScripts = glob.sync("2*-test-*up.sql", { cwd: path.resolve(migrationsPath, "sqls"), absolute: true });

            // Generate test data and fill sql scripts
            for (const scriptPath of sqlUpScripts) {
                fillSqlUpScript(scriptPath, schema);
            }

            pool = new Pool({ connectionString: process.env.POSTGRES_CONN });
            client = await pool.connect();

            await client.query(`CREATE SCHEMA IF NOT EXISTS "${schema}";`);
            await forkProcess(executablePath, ["mdb", "up", "--postgres"], {
                stdio: "ignore",
                env: {
                    PATH: process.env.PATH,
                    NODE_ENV: "test",
                    POSTGRES_CONN: process.env.POSTGRES_CONN,
                    POSTGRES_MIGRATIONS_DIR: migrationsPath,
                },
            }).catch(() => cleanUp());

            await expect(getTableCount(client, schema)).resolves.toEqual(ITERATIONS);
        });

        test("run down migrations", async () => {
            const sqlDownScripts = glob.sync("2*-test-*down.sql", { cwd: path.resolve(migrationsPath, "sqls"), absolute: true });

            for (const scriptPath of sqlDownScripts) {
                fillSqlDownScript(scriptPath, schema);
            }

            for (let i = ITERATIONS; i > 0; i--) {
                await forkProcess(executablePath, ["mdb", "down", "--postgres"], {
                    stdio: "ignore",
                    env: {
                        PATH: process.env.PATH,
                        NODE_ENV: "test",
                        POSTGRES_CONN: process.env.POSTGRES_CONN,
                        POSTGRES_MIGRATIONS_DIR: migrationsPath,
                    },
                }).catch(() => cleanUp());

                await expect(getTableCount(client, schema)).resolves.toEqual(i - 1);
            }
        });
    });
});
