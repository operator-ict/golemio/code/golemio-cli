import { fork, ForkOptions } from "child_process";
import crypto from "crypto";
import fs from "fs";
import path from "path";
import rimraf from "rimraf";

const forkProcess = async (executablePath: string, args: string[], options: ForkOptions): Promise<void> => {
    const chProcess = fork(executablePath, args, options);

    return new Promise((resolve, reject) => {
        chProcess.on("error", reject);
        chProcess.on("exit", (code) => (code === 0 ? resolve() : reject("Command exited with non-zero code")));
    });
};

/**
 * Integration test for swagger
 */
describe("I&T swagger", () => {
    const executablePath = path.resolve(__dirname, "../..", "bin/golemio.js");

    // =============================================================================
    // generate
    // =============================================================================
    describe("generate", () => {
        afterAll(() => {
            rimraf.sync(path.resolve(process.cwd(), "tmp/openapi-generated-*.json"));
        });

        test("generate merged OAS config", async () => {
            const outputFilePath = path.resolve(
                __dirname,
                `../../tmp/openapi-generated-${crypto.randomBytes(15).toString("hex")}.json`
            );

            await forkProcess(
                executablePath,
                [
                    "oas",
                    "generate",
                    "--header",
                    "test/integration/docs/openapi-header.yaml",
                    "--oas",
                    "test/integration/docs/**/openapi.yaml",
                    "--filter",
                    "test/integration/docs/openapi-tag-filter.json",
                    "--output",
                    outputFilePath,
                ],
                {
                    env: {
                        PATH: process.env.PATH,
                        NODE_ENV: "test",
                    },
                }
            );

            const generatedOas = fs.readFileSync(outputFilePath, "utf8");
            const expectedOas = fs.readFileSync(path.resolve(__dirname, "docs/openapi-expected.json"), "utf8");
            expect(generatedOas).toEqual(expectedOas);
        });
    });
});
