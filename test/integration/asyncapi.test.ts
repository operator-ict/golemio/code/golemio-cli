const { writeFileSync } = require("fs");
const bundle = require("@asyncapi/bundler");
const glob = require("fast-glob");

describe("asyncapi", () => {
    test("absolute path", async () => {
        try {
            const document = await bundle(["asyncapi_pid.yaml", "asyncapi_example.yaml"], {
                baseDir: "test/integration/example-data",
                xOrigin: true,
            });
            expect(document.yml()).toBeDefined();
            expect(document.yml()).toContain("Ping");
        } catch (error) {
            throw error;
        }
    });
});
