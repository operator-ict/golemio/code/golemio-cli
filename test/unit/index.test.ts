import { run } from "../../src";

const runMock = jest.fn();
const brandMock = jest.fn(() => ({
    src: jest.fn(() => ({
        help: jest.fn(() => ({
            defaultCommand: jest.fn(() => ({
                create: jest.fn(() => ({
                    run: runMock,
                })),
            })),
        })),
    })),
}));

jest.mock("gluegun", () => {
    return {
        build: jest.fn(() => ({
            brand: brandMock,
        })),
    };
});

/**
 * Describe index
 */
describe("UT index", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    // =============================================================================
    // run
    // =============================================================================
    describe("run", () => {
        test("run should call brand with golemio", () => {
            run([]);
            expect(brandMock).toHaveBeenCalledWith("golemio");
        });

        test("run should call run on gluegun runtime with argv", () => {
            run(["test"]);
            expect(runMock).toHaveBeenCalledWith(["test"]);
        });
    });
});
