import command from "../../../src/commands/swagger";
import * as utils from "../../../src/utils/swagger.utils";

const printMock = {
    info: jest.fn(),
    error: jest.fn(),
};

jest.mock("../../../src/utils/swagger.utils", () => {
    return {
        printCommandHelp: jest.fn(),
    };
});

/**
 * Describe swagger command
 */
describe("UT swagger command", () => {
    afterEach(() => jest.clearAllMocks());

    test("command contains certain properties", () => {
        expect(command).toHaveProperty("description", expect.any(String));
        expect(command).toHaveProperty("alias", expect.any(String));
        expect(command).toHaveProperty("run", expect.any(Function));
    });

    test("command.run should handle help action", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["help"], options: {} },
        } as any);

        expect(utils.printCommandHelp).toHaveBeenCalledTimes(1);
    });
});
