import command from "../../../src/commands/rabbitmq";
import * as utils from "../../../src/utils/rabbitmq.utils";

const printMock = {
    info: jest.fn(),
    error: jest.fn(),
    colors: { blue: jest.fn() },
};

jest.mock("../../../src/utils/rabbitmq.utils", () => {
    return {
        sendData: jest.fn(),
        receiveData: jest.fn(),
        printCommandHelp: jest.fn(),
        printCommandHelpSend: jest.fn(),
        printCommandHelpReceive: jest.fn(),
    };
});

/**
 * Describe rabbitmq command
 */
describe("UT rabbitmq command", () => {
    const oldRabbitConn = process.env.RABBIT_CONN;

    beforeEach(() => {
        process.env.RABBIT_CONN = "amqp://rabbit:pass@localhost";
    });

    afterEach(() => jest.clearAllMocks());
    afterAll(() => {
        process.env.RABBIT_CONN = oldRabbitConn;
    });

    test("command contains certain properties", () => {
        expect(command).toHaveProperty("description", expect.any(String));
        expect(command).toHaveProperty("alias", expect.any(String));
        expect(command).toHaveProperty("run", expect.any(Function));
    });

    test("command.run should handle help action", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["help"], options: {} },
        } as any);

        expect(utils.printCommandHelp).toHaveBeenCalledTimes(1);
    });

    test("command.run should null check RABBIT_CONN variable", async () => {
        process.env.RABBIT_CONN = "";
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: {} },
        } as any);

        expect(printMock.error).toHaveBeenCalledWith("Environmental variable RABBIT_CONN is not defined");
    });

    // =============================================================================
    // send action
    // =============================================================================
    describe("command.run send action", () => {
        test("command.run send action should handle help option", async () => {
            await command.run({
                print: printMock,
                parameters: { array: ["send"], options: { help: true } },
            } as any);

            expect(utils.printCommandHelpSend).toHaveBeenCalledTimes(1);
        });

        test("command.run send action should print an error and do nothing (missing exchangeName)", async () => {
            await command.run({
                print: printMock,
                parameters: { array: ["send"], options: {} },
            } as any);

            expect(printMock.error).toHaveBeenCalledWith("Specify exchange. E.g. 'dataplatform'");
        });

        test("command.run send action should print an error and do nothing (missing queueKey)", async () => {
            await command.run({
                print: printMock,
                parameters: { array: ["send"], options: { exchangeName: "test-dataplatform" } },
            } as any);

            expect(printMock.error).toHaveBeenCalledWith("Specify queueKey. E.g. 'manual.dataplatform.test.queue'");
        });

        test("command.run send action should print an error and do nothing (missing messageOrPathToFile)", async () => {
            await command.run({
                print: printMock,
                parameters: {
                    array: ["send"],
                    options: { exchangeName: "test-dataplatform", queueKey: "manual.test-dataplatform.test.queue" },
                },
            } as any);

            expect(printMock.error).toHaveBeenCalledWith("Specify messageOrPathToFile. E.g. 'Some data' or '/path/to/file.txt'");
        });

        test("command.run send action should try send message to rabbitmq", async () => {
            const exchangeName = "test-dataplatform";
            const queueKey = "manual.test-dataplatform.test.queue";
            const messageOrPathToFile = "test message";
            await command.run({
                print: printMock,
                parameters: {
                    array: ["send"],
                    options: {
                        exchange: exchangeName,
                        queueKey,
                        messageOrPathToFile,
                    },
                },
            } as any);

            expect(utils.sendData).toHaveBeenCalledTimes(1);
            expect(utils.sendData).toHaveBeenCalledWith({ exchangeName, queueKey, messageOrPathToFile });
        });
    });

    // =============================================================================
    // receive action
    // =============================================================================
    describe("command.run receive action", () => {
        test("command.run receive action should handle help option", async () => {
            await command.run({
                print: printMock,
                parameters: { array: ["receive"], options: { help: true } },
            } as any);

            expect(utils.printCommandHelpReceive).toHaveBeenCalledTimes(1);
        });

        test("command.run receive action should print an error and do nothing (missing queueName)", async () => {
            await command.run({
                print: printMock,
                parameters: { array: ["receive"], options: {} },
            } as any);

            expect(printMock.error).toHaveBeenCalledWith("Specify queueName. E.g. 'dataplatform.test.queue'");
        });

        test("command.run receive action should try receive message from rabbitmq", async () => {
            const queueName = "test-dataplatform.test.queue";
            await command.run({
                print: printMock,
                parameters: {
                    array: ["receive"],
                    options: {
                        queueName,
                    },
                },
            } as any);

            expect(utils.receiveData).toHaveBeenCalledTimes(1);
            expect(utils.receiveData).toHaveBeenCalledWith({ queueName });
        });

        test("command.run receive action should try receive message from rabbitmq and dequeue message", async () => {
            const queueName = "test-dataplatform.test.queue";
            const dequeue = "true";
            await command.run({
                print: printMock,
                parameters: {
                    array: ["receive"],
                    options: {
                        queueName,
                        dequeue,
                    },
                },
            } as any);

            expect(utils.receiveData).toHaveBeenCalledTimes(1);
            expect(utils.receiveData).toHaveBeenCalledWith({ queueName, dequeue: true });
        });
    });
});
