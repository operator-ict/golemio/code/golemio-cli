import command from "../../../src/commands/import-db-data";
import * as utils from "../../../src/utils/import-db-data.utils";

const printMock = {
    info: jest.fn(),
    warning: jest.fn(),
    error: jest.fn(),
    colors: { blue: jest.fn() },
};

jest.mock("../../../src/utils/import-db-data.utils", () => {
    return {
        importPostgresData: jest.fn(),
        extractDangerousWords: jest.fn().mockReturnValue([]),
    };
});

/**
 * Describe import-db-data command
 */
describe("UT import-db-data command", () => {
    const oldPostgresConn = process.env.POSTGRES_CONN;

    beforeEach(() => {
        process.env.POSTGRES_CONN = "postgres://oict:oict-pass@localhost/oict-test";
    });

    afterEach(() => jest.clearAllMocks());
    afterAll(() => {
        process.env.POSTGRES_CONN = oldPostgresConn;
    });

    test("command contains certain properties", () => {
        expect(command).toHaveProperty("description", expect.any(String));
        expect(command).toHaveProperty("alias", expect.any(String));
        expect(command).toHaveProperty("run", expect.any(Function));
    });

    test("command.run should handle help action", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["help"], options: {} },
        } as any);

        expect(printMock.colors.blue).toHaveBeenCalled();
        expect(printMock.info).toHaveBeenCalledTimes(7);
    });

    test("command.run should null check POSTGRES_CONN variable", async () => {
        process.env.POSTGRES_CONN = "";
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: { postgres: true } },
        } as any);

        expect(printMock.error).toHaveBeenCalledWith("Environmental variable POSTGRES_CONN is not defined");
    });

    test("command.run should check dangerous words and print error", async () => {
        (utils.extractDangerousWords as jest.Mock).mockReturnValueOnce(["test"]);
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: { postgres: true } },
        } as any);

        expect(printMock.error).toHaveBeenNthCalledWith(
            1,
            "Environmental variable POSTGRES_CONN contains dangerous words: [test]"
        );
        expect(printMock.error).toHaveBeenNthCalledWith(2, "Suppress this error message with --dangerous");
        expect(utils.importPostgresData).not.toHaveBeenCalled();
    });

    test("command.run should try importing postgres data", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: { postgres: true } },
        } as any);

        expect(utils.importPostgresData).toHaveBeenCalledWith(undefined);
    });

    test("command.run should try importing postgres data (dangerous mode)", async () => {
        (utils.extractDangerousWords as jest.Mock).mockReturnValueOnce(["test"]);
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: { dangerous: true } },
        } as any);

        expect(printMock.warning).toHaveBeenCalledWith("Dangerous mode is active");
        expect(printMock.error).not.toHaveBeenCalled();
        expect(utils.importPostgresData).toHaveBeenCalledWith(undefined);
    });
});
