import path from "path";
import glob from "fast-glob";
import { strings } from "gluegun";
import * as utils from "../../../src/utils/migrate-db.utils";
import command from "../../../src/commands/migrate-db";

const printMock = {
    info: jest.fn(),
    warning: jest.fn(),
    error: jest.fn(),
    table: jest.fn(),
    colors: { blue: jest.fn(), green: jest.fn((text) => text + "(:green)"), red: jest.fn((text) => text + "(:red)") },
};

const migrationMock = {
    silence: jest.fn(),
    create: jest.fn().mockResolvedValue(null),
    up: jest.fn().mockResolvedValue(null),
    down: jest.fn().mockResolvedValue(null),
    reset: jest.fn().mockResolvedValue(null),
};

const isDirectoryMock = jest.fn().mockReturnValue(true);

jest.mock("fs", () => {
    return {
        ...jest.requireActual("fs"),
        existsSync: jest.fn().mockReturnValue(true),
        lstatSync: jest.fn(() => ({
            isDirectory: isDirectoryMock,
        })),
    };
});

jest.mock("path", () => {
    return {
        ...jest.requireActual("path"),
        resolve: jest.fn().mockReturnValue("test"),
    };
});

jest.mock("fast-glob", () => {
    return {
        sync: jest.fn(),
    };
});

jest.mock("../../../src/utils/migrate-db.utils", () => {
    return {
        getDefaultModulePgSchemas: jest.fn(async () => new Map()),
        getPostgresMigrateInstances: jest.fn(() => ({ migrations: [migrationMock], schemas: ["test"] })),
        readLockfile: jest.fn(() => ({
            packages: {},
        })),
        printCommandHelp: jest.fn(),
        printSummaryMatrix: jest.fn(),
    };
});

/**
 * Describe migrate-db command
 */
describe("UT migrate-db command", () => {
    const oldProcess = process;
    const oldPostgresConn = process.env.POSTGRES_CONN;

    beforeAll(
        () =>
            (global.process = {
                ...oldProcess,
                getMaxListeners: jest.fn().mockReturnValue(0),
                setMaxListeners: jest.fn(),
                exit: jest.fn() as any,
            })
    );

    beforeEach(() => (process.env.POSTGRES_CONN = "postgres://oict:oict-pass@localhost/oict-test"));
    afterEach(() => jest.clearAllMocks());
    afterAll(() => {
        global.process = oldProcess;
        process.env.POSTGRES_CONN = oldPostgresConn;
    });

    test("command contains certain properties", () => {
        expect(command).toHaveProperty("description", expect.any(String));
        expect(command).toHaveProperty("alias", expect.any(String));
        expect(command).toHaveProperty("run", expect.any(Function));
    });

    test("command.run should handle help action", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["help"], options: {} },
        } as any);

        expect(utils.printCommandHelp).toHaveBeenCalledTimes(1);
    });

    // =============================================================================
    // create action
    // =============================================================================
    describe("command.run create action", () => {
        test("command.run create action should reject migration glob (postgres)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("**.kekw");
            await command.run({
                print: printMock,
                parameters: { array: ["create", "testName"], options: { postgres: true } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(1);
            expect(printMock.error).toHaveBeenCalledWith("Multiple paths / globs are not supported for this action");
        });

        test("command.run create action should call getPostgresMigrateInstances (postgres)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/migrations");

            await command.run({
                print: printMock,
                parameters: { array: ["create", "testName"], options: { postgres: true } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(1);

            expect(utils.getPostgresMigrateInstances).toHaveBeenCalledWith({
                connectionString: process.env.POSTGRES_CONN,
                migrationDir: "test/migrations",
            });

            expect(migrationMock.create).toHaveBeenCalledWith("testName");
        });

        test(
            "command.run create action should call getPostgresMigrateInstances with custom migration name " + "(postgres)",
            async () => {
                (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/migrations");

                const promptMock = {
                    ask: jest.fn().mockReturnValue({ newMigrationName: " newTest Migration  " }),
                };

                const kebabCaseMock = jest.spyOn(strings, "kebabCase");
                await command.run({
                    print: printMock,
                    prompt: promptMock,
                    strings,
                    parameters: { array: ["create"], options: { postgres: true } },
                } as any);

                expect(path.resolve).toHaveBeenCalledTimes(1);

                expect(promptMock.ask).toHaveBeenCalledWith(expect.any(Array));
                expect(kebabCaseMock).toHaveBeenCalledWith("newTest Migration");
                expect(utils.getPostgresMigrateInstances).toHaveBeenCalledWith({
                    connectionString: process.env.POSTGRES_CONN,
                    migrationDir: "test/migrations",
                });

                expect(migrationMock.create).toHaveBeenCalledWith("new-test-migration");
            }
        );
    });

    // =============================================================================
    // up/down/reset action
    // =============================================================================
    describe("command.run up/down/reset action", () => {
        test("command.run up action should print an error and do nothing (postgres, zero migration directories)", async () => {
            (glob.sync as unknown as jest.Mock).mockReturnValueOnce([]);
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/**/migrations");

            await command.run({
                print: printMock,
                parameters: { array: ["up"], options: { postgres: true } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(1);
            expect(printMock.error).toHaveBeenCalledWith("Zero migration directories");
        });

        test("command.run up action should null check POSTGRES_CONN variable (postgres)", async () => {
            process.env.POSTGRES_CONN = "";
            await command.run({
                print: printMock,
                parameters: { array: ["up"], options: { postgres: true } },
            } as any);

            expect(printMock.error).toHaveBeenCalledWith("Environmental variable POSTGRES_CONN is not defined");
        });

        test(
            "command.run up should not allow a non-existent single directory to pass (postgres, single " + "migration directory)",
            async () => {
                isDirectoryMock.mockReturnValueOnce(false);
                (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/migrations");

                await command.run({
                    print: printMock,
                    parameters: { array: ["up"], options: { postgres: true } },
                } as any);

                expect(path.resolve).toHaveBeenCalledTimes(1);

                expect(printMock.warning).toHaveBeenCalledWith("Migrations directory test/migrations does not exist");
                expect(printMock.error).toHaveBeenCalledWith("Zero migration directories");
            }
        );

        test("command.run up action should call getPostgresMigrateInstances (postgres, single migration directory)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/migrations");

            await command.run({
                print: printMock,
                parameters: { array: ["up"], options: { postgres: true } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(1);

            expect(utils.readLockfile).toHaveBeenCalledTimes(1);
            expect(utils.getPostgresMigrateInstances).toHaveBeenCalledTimes(1);
            expect(utils.getPostgresMigrateInstances).toHaveBeenCalledWith(
                expect.objectContaining({
                    connectionString: process.env.POSTGRES_CONN,
                    defaultModulePgSchemas: expect.any(Map),
                    migrationDir: `test/migrations`,
                    includeLocalConfig: true,
                    lockfileData: expect.objectContaining({ packages: expect.anything() }),
                })
            );

            expect(migrationMock.up).toHaveBeenCalledTimes(1);
            expect(process.setMaxListeners).toHaveBeenCalledWith(2);
        });

        test(
            "command.run up action should call getPostgresMigrateInstance (postgres, multiple migration " + "directories)",
            async () => {
                const migrationDirs = ["test/0/migrations", "test/1/migrations"];
                (glob.sync as unknown as jest.Mock).mockReturnValueOnce(migrationDirs);
                (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/**/migrations");

                await command.run({
                    print: printMock,
                    parameters: { array: ["up"], options: { path: "test/**/migrations" } },
                } as any);

                expect(path.resolve).toHaveBeenCalledTimes(1);

                expect(utils.readLockfile).toHaveBeenCalledTimes(1);
                expect(utils.getPostgresMigrateInstances).toHaveBeenCalledTimes(migrationDirs.length);

                for (let i = 0; i < migrationDirs.length; i++) {
                    expect(utils.getPostgresMigrateInstances).toHaveBeenNthCalledWith(
                        i + 1,
                        expect.objectContaining({
                            connectionString: process.env.POSTGRES_CONN,
                            defaultModulePgSchemas: expect.any(Map),
                            migrationDir: `test/${i}/migrations`,
                            includeLocalConfig: true,
                            lockfileData: expect.objectContaining({ packages: expect.anything() }),
                        })
                    );
                }

                expect(migrationMock.up).toHaveBeenCalledTimes(migrationDirs.length);
                expect(process.setMaxListeners).toHaveBeenCalledWith(3);
            }
        );

        test("command.run up action should call getPostgresMigrateInstances (postgres, multiple migration paths)", async () => {
            const migrationDirs = ["test/0/migrations", "test/1/migrations"];
            (glob.sync as unknown as jest.Mock).mockReturnValueOnce(migrationDirs);
            (path.resolve as unknown as jest.Mock)
                .mockReturnValueOnce("test/**/migrations")
                .mockReturnValueOnce("tests/migrations");

            await command.run({
                print: printMock,
                parameters: { array: ["up"], options: { path: ["test/**/migrations", "tests/migrations"] } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(2);

            expect(utils.readLockfile).toHaveBeenCalledTimes(1);
            expect(utils.getPostgresMigrateInstances).toHaveBeenCalledTimes(3);

            for (let i = 0; i < migrationDirs.length; i++) {
                expect(utils.getPostgresMigrateInstances).toHaveBeenNthCalledWith(
                    i + 1,
                    expect.objectContaining({
                        connectionString: process.env.POSTGRES_CONN,
                        defaultModulePgSchemas: expect.any(Map),
                        migrationDir: `test/${i}/migrations`,
                        includeLocalConfig: true,
                        lockfileData: expect.objectContaining({ packages: expect.anything() }),
                    })
                );
            }

            expect(utils.getPostgresMigrateInstances).toHaveBeenNthCalledWith(migrationDirs.length + 1, {
                connectionString: process.env.POSTGRES_CONN,
                defaultModulePgSchemas: expect.any(Map),
                migrationDir: `tests/migrations`,
                includeLocalConfig: true,
                lockfileData: expect.objectContaining({ packages: expect.anything() }),
            });

            expect(migrationMock.up).toHaveBeenCalledTimes(3);
            expect(process.setMaxListeners).toHaveBeenCalledWith(4);
        });

        test("command.run up action should ignore stdout (postgres, silent mode)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/migrations");
            (utils.getPostgresMigrateInstances as unknown as jest.Mock).mockImplementationOnce(
                jest.fn(() => {
                    console.log("test");
                    return { migrations: [migrationMock], schemas: ["test"] };
                })
            );

            const stdoutWriteStream = process.stdout.write;
            process.stdout.write = jest.fn();

            await command.run({
                print: printMock,
                parameters: { array: ["up"], options: { postgres: true, silent: true } },
            } as any);

            expect(path.resolve).toHaveBeenCalledTimes(1);

            expect(process.stdout.write).toHaveBeenCalledTimes(0);
            process.stdout.write = stdoutWriteStream;

            expect(process.setMaxListeners).toHaveBeenCalledWith(2);
        });
    });
});
