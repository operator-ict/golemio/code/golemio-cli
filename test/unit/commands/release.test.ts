import command from "../../../src/commands/release";
import * as utils from "../../../src/utils/release.utils";

const printMock = {
    info: jest.fn(),
    error: jest.fn(),
    highlight: jest.fn(),
    colors: {
        blue: jest.fn(),
        yellow: jest.fn(),
        bgYellow: jest.fn(),
        bgGreen: jest.fn(),
        bgRed: jest.fn(),
    },
};

jest.mock("../../../src/utils/release.utils", () => {
    return {
        check: jest.fn(() => ({
            someDiffs: true,
            diff: { changed: 1, deletions: 0, insertions: 0, files: ["test"] },
            changelog: "",
        })),
        filterIds: jest.fn((ids) => ids),
        getGitlabRepo: jest.fn(() => ({ id: "1234", name: "test", web_url: "https://test.url.com", archived: false })),
        printCommandHelp: jest.fn(),
        ReleaseScopeEnum: { core: "core" },
    };
});

jest.mock("../../../src/constants/release-module-ids.constant.ts", () => ["24157672"]);

/**
 * Describe release command
 */
describe("UT release command", () => {
    const oldGitlabApiToken = process.env.GITLAB_API_TOKEN;
    const oldBranchName = process.env.RELEASE_BRANCH_NAME;

    beforeEach(() => {
        process.env.GITLAB_API_TOKEN = "token";
        process.env.RELEASE_BRANCH_NAME = "test-release";
    });

    afterEach(() => jest.clearAllMocks());
    afterAll(() => {
        process.env.GITLAB_API_TOKEN = oldGitlabApiToken;
        process.env.RELEASE_BRANCH_NAME = oldBranchName;
    });

    test("command contains certain properties", () => {
        expect(command).toHaveProperty("description", expect.any(String));
        expect(command).toHaveProperty("alias", expect.any(String));
        expect(command).toHaveProperty("run", expect.any(Function));
    });

    test("command.run should handle help action", async () => {
        await command.run({
            print: printMock,
            parameters: { array: ["help"], options: { scope: "core" } },
        } as any);

        expect(utils.printCommandHelp).toHaveBeenCalledTimes(1);
    });

    test("command.run should null check GITLAB_API_TOKEN variable", async () => {
        process.env.GITLAB_API_TOKEN = "";
        await command.run({
            print: printMock,
            parameters: { array: ["whatever"], options: {} },
        } as any);

        expect(printMock.error).toHaveBeenCalledWith("Environmental variable GITLAB_API_TOKEN is not defined");
    });
});
