import crossSpawn from "cross-spawn";
import dbm from "db-migrate";
import fs from "fs";
import { print } from "gluegun";
import { readFile } from "node:fs/promises";
import path from "path";
import {
    CommandResult,
    getDefaultModulePgSchemas,
    getPostgresMigrateInstances,
    printCommandHelp,
    printSummaryMatrix,
    runCommandShim,
} from "../../../src/utils/migrate-db.utils";

jest.mock("fs", () => {
    return {
        ...jest.requireActual("fs"),
        existsSync: jest.fn(),
        readFileSync: jest.fn().mockReturnValue(""),
        writeFileSync: jest.fn(),
    };
});

jest.mock("node:fs/promises", () => {
    return {
        ...jest.requireActual("node:fs/promises"),
        readFile: jest.fn().mockReturnValue(Promise.resolve("")),
    };
});

jest.mock("path", () => {
    return {
        ...jest.requireActual("path"),
        resolve: jest.fn(),
        relative: jest.fn((_cwd, dir) => dir),
    };
});

jest.mock("cross-spawn", () => {
    return {
        spawn: jest.fn(),
    };
});

jest.mock("db-migrate", () => {
    return {
        getInstance: jest.fn(),
    };
});

jest.mock("gluegun", () => {
    return {
        ...jest.requireActual("gluegun"),
        print: {
            info: jest.fn(),
            warning: jest.fn(),
            table: jest.fn(),
            colors: { blue: jest.fn(), green: jest.fn((text) => text + "(:green)"), red: jest.fn((text) => text + "(:red)") },
        },
    };
});

/**
 * Describe migrate-db.utils
 */
describe("UT migrate-db.utils", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    // =============================================================================
    // runCommandShim
    // =============================================================================
    describe("runCommandShim", () => {
        test("runCommandShim should reject (error)", async () => {
            (crossSpawn as any).spawn = jest.fn(() => ({
                on: jest.fn().mockImplementationOnce((_ev: string, reject: Function) => reject()),
            }));
            await expect(runCommandShim("npm", "test me")).rejects.toBeUndefined();
        });

        test("runCommandShim should reject (non-zero exit code)", async () => {
            (crossSpawn as any).spawn = jest.fn(() => ({
                on: jest
                    .fn()
                    .mockImplementationOnce(() => {})
                    .mockImplementationOnce((_ev: string, cb: Function) => cb(1)),
            }));
            await expect(runCommandShim("npm", "test me")).rejects.toEqual("Command exited with non-zero code");
        });

        test("runCommandShim should resolve", async () => {
            (crossSpawn as any).spawn = jest.fn(() => ({
                on: jest
                    .fn()
                    .mockImplementationOnce(() => {})
                    .mockImplementationOnce((_ev: string, cb: Function) => cb(0)),
            }));
            await expect(runCommandShim("npm", "test me")).resolves.toBeUndefined();
        });
    });

    // =============================================================================
    // getDefaultModulePgSchemas
    // =============================================================================
    describe("getDefaultModulePgSchemas", () => {
        test("should return default module schemas (module with audit logs and own migrations scenario)", async () => {
            (path.resolve as unknown as jest.Mock).mockImplementation((...args) => args.join("/"));
            (fs.existsSync as unknown as jest.Mock).mockImplementation((path) =>
                [
                    "/home/username/pid/db/migrations/postgresql/.config.json",
                    "/home/username/pid/node_modules/@golemio/audit-logs/db/migrations/postgresql/.config.json",
                ].includes(path)
            );
            (readFile as unknown as jest.Mock).mockImplementation(async (path) => {
                switch (path) {
                    case "/home/username/pid/node_modules/@golemio/audit-logs/db/migrations/postgresql/.config.json":
                        return JSON.stringify({ schema: "$DEFAULT_MODULE_PG_SCHEMA" });
                    case "/home/username/pid/db/migrations/postgresql/.config.json":
                        return JSON.stringify({
                            schema: "pid, public",
                            isSchemaModuleDefault: true,
                        });
                    default:
                        throw new Error("Unexpected file");
                }
            });

            const result = await getDefaultModulePgSchemas([
                "/home/username/pid/db/migrations/postgresql",
                "/home/username/pid/node_modules/@golemio/audit-logs/db/migrations/postgresql",
            ]);

            expect(result).toEqual(new Map([["", "pid, public"]]));
        });

        test("should return default module schemas (service with multiple modules and audit logs scenario)", async () => {
            (path.resolve as unknown as jest.Mock).mockImplementation((...args) => args.join("/"));
            (fs.existsSync as unknown as jest.Mock).mockImplementation((path) =>
                [
                    "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql/.config.json",
                    "/home/username/integration-engine/node_modules/@golemio/fcd/db/migrations/postgresql/.config.json",
                    "/home/username/integration-engine/node_modules/@golemio/pid/db/migrations/postgresql/.config.json",
                    "/home/username/integration-engine/node_modules/@golemio/ndic/db/migrations/postgresql/.config.json",
                ].includes(path)
            );
            (readFile as unknown as jest.Mock).mockImplementation(async (path) => {
                switch (path) {
                    case "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql" +
                        "/.config.json":
                        return JSON.stringify({ schema: "$DEFAULT_MODULE_PG_SCHEMA" });
                    case "/home/username/integration-engine/node_modules/@golemio/fcd/db/migrations/postgresql/.config.json":
                        return JSON.stringify({
                            schema: "fcd, public",
                            isSchemaModuleDefault: true,
                        });
                    case "/home/username/integration-engine/node_modules/@golemio/pid/db/migrations/postgresql/.config.json":
                        return JSON.stringify({
                            schema: "pid, public",
                            isSchemaModuleDefault: true,
                        });
                    case "/home/username/integration-engine/node_modules/@golemio/ndic/db/migrations/postgresql/.config.json":
                        return JSON.stringify({ schema: "ndic, public" });
                    default:
                        throw new Error("Unexpected file");
                }
            });

            const result = await getDefaultModulePgSchemas([
                "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql",
                "/home/username/integration-engine/node_modules/@golemio/fcd/db/migrations/postgresql",
                "/home/username/integration-engine/node_modules/@golemio/pid/db/migrations/postgresql",
                "/home/username/integration-engine/node_modules/@golemio/ndic/db/migrations/postgresql",
            ]);

            expect(result).toEqual(
                new Map([
                    ["node_modules/@golemio/fcd", "fcd, public"],
                    ["node_modules/@golemio/pid", "pid, public"],
                ])
            );
        });
    });

    // =============================================================================
    // getPostgresMigrateInstances
    // =============================================================================
    describe("getPostgresMigrateInstances", () => {
        test("getPostgresMigrateInstances should call getInstance (default config)", async () => {
            getPostgresMigrateInstances({
                connectionString: "postgres://test:test-pass@localhost:6969/test-db",
                migrationDir: "test/migrations",
            });

            expect(dbm.getInstance).toHaveBeenCalledWith(true, {
                config: {
                    postgres: {
                        driver: "pg",
                        user: "test",
                        password: "test-pass",
                        host: "localhost",
                        database: "test-db",
                        port: "6969",
                        ssl: false,
                        schema: "public",
                    },
                },
                cmdOptions: {
                    "migrations-dir": "test/migrations",
                    "sql-file": true,
                },
                env: "postgres",
            });
        });

        test("getPostgresMigrateInstances should call getInstance (local non-existent config)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/config.json");
            (fs.existsSync as unknown as jest.Mock).mockReturnValueOnce(false);

            getPostgresMigrateInstances({
                connectionString: "postgres://test:test-pass@localhost/test-db",
                migrationDir: "test/migrations",
                defaultSchema: "sth",
                includeLocalConfig: true,
            });

            expect(print.warning).toHaveBeenCalledWith(expect.stringContaining("test/config.json"));
            expect(dbm.getInstance).toHaveBeenCalledWith(true, {
                config: {
                    postgres: {
                        driver: "pg",
                        user: "test",
                        password: "test-pass",
                        host: "localhost",
                        database: "test-db",
                        port: "5432",
                        ssl: false,
                        schema: "sth",
                    },
                },
                cmdOptions: {
                    "migrations-dir": "test/migrations",
                    "sql-file": true,
                },
                env: "postgres",
            });
        });

        test("getPostgresMigrateInstances should call getInstance (local config)", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/config.json");
            (fs.existsSync as unknown as jest.Mock).mockReturnValueOnce(true);

            const originalJsonParse = JSON.parse;
            JSON.parse = jest.fn().mockReturnValue({ schema: "test" });

            getPostgresMigrateInstances({
                connectionString: "postgres://test:test-pass@localhost/test-db",
                migrationDir: "test/migrations",
                includeLocalConfig: true,
            });

            expect(dbm.getInstance).toHaveBeenCalledWith(true, {
                config: {
                    postgres: {
                        driver: "pg",
                        user: "test",
                        password: "test-pass",
                        host: "localhost",
                        database: "test-db",
                        port: "5432",
                        ssl: false,
                        schema: "test",
                    },
                },
                cmdOptions: {
                    "migrations-dir": "test/migrations",
                    "sql-file": true,
                },
                env: "postgres",
            });

            JSON.parse = originalJsonParse;
        });

        test("getPostgresMigrateInstances->createLocalConfig should create configuration file", async () => {
            const writeFileMock = fs.writeFileSync as unknown as jest.Mock;
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/config.json");
            (fs.existsSync as unknown as jest.Mock).mockReturnValueOnce(false);

            const { createLocalConfig } = getPostgresMigrateInstances({
                connectionString: "postgres://test:test-pass@localhost:6969/test-db",
                migrationDir: "test/migrations",
                defaultSchema: "test",
            });

            createLocalConfig();
            expect(writeFileMock).toHaveBeenCalledWith("test/config.json", expect.stringMatching(/schema.{1,}:.{1,}"test/gm));
        });

        test("should call getInstance correctly (local config, default module schema) ", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/config.json");
            (fs.existsSync as unknown as jest.Mock).mockReturnValueOnce(true);

            const originalJsonParse = JSON.parse;
            JSON.parse = jest.fn().mockReturnValue({ schema: "$DEFAULT_MODULE_PG_SCHEMA" });

            getPostgresMigrateInstances({
                connectionString: "postgres://test:test-pass@localhost/test-db",
                migrationDir: "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql",
                defaultModulePgSchemas: new Map([["", "pid, public"]]),
                includeLocalConfig: true,
                lockfileData: {
                    name: "@golemio/pid",
                    version: "3.2.1",
                    lockfileVersion: 3,
                    requires: true,
                    packages: {
                        "": {
                            name: "@golemio/pid",
                            version: "3.2.1",
                            dependencies: {
                                "@golemio/audit-logs": "file:../audit-logs",
                            },
                        },
                        "../audit-logs": {
                            name: "@golemio/audit-logs",
                            version: "1.0.0",
                        },
                        "node_modules/@golemio/audit-logs": {
                            resolved: "../audit-logs",
                            link: true,
                        },
                    },
                },
            });

            expect(dbm.getInstance).toHaveBeenCalledWith(true, {
                config: {
                    postgres: {
                        driver: "pg",
                        user: "test",
                        password: "test-pass",
                        host: "localhost",
                        database: "test-db",
                        port: "5432",
                        ssl: false,
                        schema: "pid, public",
                    },
                },
                cmdOptions: {
                    "migrations-dir":
                        "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql",
                    "sql-file": true,
                },
                env: "postgres",
            });

            JSON.parse = originalJsonParse;
        });

        test("should throw an error when using unsupported lockfile version (local config, default module schema) ", async () => {
            (path.resolve as unknown as jest.Mock).mockReturnValueOnce("test/config.json");
            (fs.existsSync as unknown as jest.Mock).mockReturnValueOnce(true);

            const originalJsonParse = JSON.parse;
            JSON.parse = jest.fn().mockReturnValue({ schema: "$DEFAULT_MODULE_PG_SCHEMA" });

            expect(() =>
                getPostgresMigrateInstances({
                    connectionString: "postgres://test:test-pass@localhost/test-db",
                    migrationDir: "/home/username/integration-engine/node_modules/@golemio/audit-logs/db/migrations/postgresql",
                    defaultModulePgSchemas: new Map([["", "pid, public"]]),
                    includeLocalConfig: true,
                    lockfileData: {
                        name: "@golemio/pid",
                        version: "3.2.1",
                        lockfileVersion: 1e9,
                        requires: true,
                        packages: {
                            "": {
                                name: "@golemio/pid",
                                version: "3.2.1",
                                dependencies: {
                                    "@golemio/audit-logs": "file:../audit-logs",
                                },
                            },
                            "../audit-logs": {
                                name: "@golemio/audit-logs",
                                version: "1.0.0",
                            },
                            "node_modules/@golemio/audit-logs": {
                                resolved: "../audit-logs",
                                link: true,
                            },
                        },
                    },
                })
            ).toThrowError(Error);

            JSON.parse = originalJsonParse;
        });
    });

    // =============================================================================
    // printCommandHelp
    // =============================================================================
    describe("printCommandHelp", () => {
        test("printCommandHelp should print help for the command", async () => {
            printCommandHelp();
            expect(print.info).toHaveBeenCalledTimes(12);
        });
    });

    // =============================================================================
    // printSummaryMatrix
    // =============================================================================
    describe("printSummaryMatrix", () => {
        test("printSummaryMatrix should transform results and print summary matrix", async () => {
            const results: CommandResult[] = [
                ["node_modules/@golemio/test/test/0/migrations", "test", true],
                ["test/1/migrations", "public", true],
                ["test/2/migrations", "test, public", true],
                ["test/3/migrations", "public, test", false, "test"],
            ];

            printSummaryMatrix(results);
            expect(print.table).toHaveBeenCalledWith(
                [
                    ["Location", "Schema", "Result", "Info"],
                    ["Module: @golemio/test", "test", "Success(:green)", "\uD83E\uDD73"],
                    ["Path: test/1/migrations", "public", "Success(:green)", "\uD83E\uDD73"],
                    ["Path: test/2/migrations", "test", "Success(:green)", "\uD83E\uDD73"],
                    ["Path: test/3/migrations", "public, test", "Fail(:red)", "test"],
                ],
                expect.any(Object)
            );
        });
    });
});
