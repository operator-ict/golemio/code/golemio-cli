import glob from "fast-glob";
import { print } from "gluegun";
import { importPostgresData, extractDangerousWords } from "../../../src/utils/import-db-data.utils";

const queryMock = jest.fn(() => ({ rows: [{ search_path: { split: jest.fn(() => ["a"]) } }] }));

jest.mock("fast-glob", () => {
    return {
        sync: jest.fn(),
    };
});

jest.mock("pg", () => {
    return {
        Pool: jest.fn(() => ({
            connect: jest.fn(() => ({
                query: queryMock,
                release: jest.fn(),
            })),
            end: jest.fn(),
        })),
    };
});

jest.mock("fs", () => {
    return {
        ...jest.requireActual("fs"),
        readFileSync: jest.fn().mockReturnValue("test"),
    };
});

jest.mock("gluegun", () => {
    return {
        ...jest.requireActual("gluegun"),
        print: {
            error: jest.fn(),
            info: jest.fn(),
            highlight: jest.fn(),
            divider: jest.fn(),
        },
    };
});

/**
 * Describe import-db-data.utils
 */
describe("UT import-db-data.utils", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    // =============================================================================
    // importPostgresData
    // =============================================================================
    describe("importPostgresData", () => {
        test("importPostgresData should return early", async () => {
            (glob as any).sync.mockReturnValueOnce([]);

            await importPostgresData();
            expect(print.error).toHaveBeenCalledWith("No SQL data to import");
        });

        test("importPostgresData should run queries", async () => {
            (glob as any).sync.mockReturnValueOnce(["test", "test", "test"]);

            await importPostgresData();
            expect(print.info).toHaveBeenCalledTimes(3);
            expect(print.info).toHaveBeenCalledWith("Restoring SQL data from 'test'");
            expect(queryMock).toHaveBeenCalledTimes(5);
            expect(queryMock).toHaveBeenCalledWith("test");
        });
    });

    // =============================================================================
    // extractDangerousWords
    // =============================================================================
    describe("extractDangerousWords", () => {
        test("extractDangerousWords should return word list", async () => {
            const wordList = extractDangerousWords("postgres://oict:oict-pass@golem.host/oict-test");
            expect(wordList).toStrictEqual(["golem"]);
        });

        test("extractDangerousWords should return empty list", async () => {
            const wordList = extractDangerousWords("postgres://oict:oict-pass@local.host/oict-test");
            expect(wordList).toStrictEqual([]);
        });
    });
});
