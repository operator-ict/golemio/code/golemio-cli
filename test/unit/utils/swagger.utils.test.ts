import * as portmanModule from "@apideck/portman/dist/Portman";
import fs from "fs";
import path from "path";
import { runPortmanTests } from "../../../src/utils/swagger.utils";

jest.mock("@apideck/portman/dist/Portman", () => {
    return {
        Portman: jest.fn().mockImplementation(() => {
            return {
                run: jest.fn().mockResolvedValue({}),
            };
        }),
    };
});

/**
 * Describe swagger.utils
 */
describe("UT swagger.utils", () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.resetModules();
        fs.unlinkSync(path.resolve(process.cwd(), "test.js"));
    });

    describe("runPortmanTests", () => {
        test("runPortmanTests should succeed", async () => {
            fs.writeFileSync(
                path.resolve(process.cwd(), "test.js"),
                `module.exports = {
                  start: async () => {},
                  stop: async () => {},
                }`
            );
            const result = await runPortmanTests("test.yaml", null, "test.js", null);
            expect(result).toEqual({ isError: false, message: "Successfully ran Portman tests" });
            expect(portmanModule.Portman).toHaveBeenNthCalledWith(1, {
                baseUrl: "http://localhost:3011",
                bundleContractTests: false,
                envFile: ".env",
                filterFile: path.resolve(__dirname, "..", "..", "..", "static", "portman-filter.json"),
                includeTests: true,
                newmanIterationData: "",
                oaLocal: path.resolve(process.cwd(), "test.yaml"),
                oaOutput: path.resolve(process.cwd(), "docs/postman-output-collection-generated.json"),
                oaUrl: "",
                portmanConfigFile: "",
                portmanConfigPath: path.resolve(__dirname, "..", "..", "..", "static", "portman-config.json"),
                postmanConfigFile: "",
                postmanConfigPath: path.resolve(__dirname, "..", "..", "..", "static", "postman-config.json"),
                runNewman: true,
                syncPostman: false,
            });
        });

        test("runPortmanTests should fail - missing start/stop script", async () => {
            fs.writeFileSync(path.resolve(process.cwd(), "test.js"), `module.exports = {}`);
            const result = await runPortmanTests("test.yaml", null, "test.js", null);
            expect(result).toEqual({ isError: true, message: "Invalid Portman script" });
            expect(portmanModule.Portman).toHaveBeenCalledTimes(0);
        });
    });
});
