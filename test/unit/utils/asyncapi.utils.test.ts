import { generateAsyncApiConfig } from "./../../../src/utils/asyncapi.utils";
import { uploadFile } from "./../../../src/utils/asyncapi.utils";

jest.mock("@azure/identity", () => {
    return {
        ClientSecretCredential: jest.fn(() => ({
            getToken: jest.fn(() => ({
                token: "token",
                expiresOnTimestamp: 123456789,
            })),
        })),
    };
});

jest.mock("@azure/storage-blob", () => {
    return {
        BlobServiceClient: jest.fn(() => ({
            getContainerClient: jest.fn(() => ({
                getBlockBlobClient: jest.fn(() => ({
                    uploadData: jest.fn(),
                })),
            })),
            end: jest.fn(),
        })),
        BlockBlobParallelUploadOptions: jest.fn(() => ({
            blockSize: 4 * 1024 * 1024,
            maxSingleShotSize: 100 * 1024 * 1024,
            maxConcurrency: 20,
        })),
    };
});

describe("asyncapi tests", () => {
    it("should return a successful result when valid arguments are provided", async () => {
        const oasHeaderFilePath = null;
        const oasFilesGlob = "test/integration/example-data/*.yaml";
        const oasMergedOutputFilePath = "test/unit/example-data/result/merged.yaml";

        const result = await generateAsyncApiConfig(oasHeaderFilePath, oasFilesGlob, oasMergedOutputFilePath);
        expect(result.isError).toBe(false);
        expect(result.message).toBe("Successfully generated asyncapi file at test/unit/example-data/result/merged.yaml");
    });

    it("should return an error result when oasFilesGlob is null", async () => {
        const oasHeaderFilePath = null;
        const oasFilesGlob = "bla";
        const oasMergedOutputFilePath = "/path/to/oas/output.yaml";

        const result = await generateAsyncApiConfig(oasHeaderFilePath, oasFilesGlob, oasMergedOutputFilePath);

        expect(result.isError).toBe(true);
        expect(result.message).toBe("Cannot read properties of undefined (reading 'asyncapi')");
    });

    it("upload asyncapi file to azure", async () => {
        const input = "test/unit/example-data/asyncapi_example.yaml";
        const output = "test/unit/example-data/result";

        const result = await uploadFile(input, output);
        const file = `${output}/index.html`;

        expect(result.isError).toBe(false);
        expect(result.message).toBe(`File ${file} uploaded.`);
    }, 80000);
});
