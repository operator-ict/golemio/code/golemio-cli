import { print } from "gluegun";
import {
    printCommandHelp,
    check,
    getGitlabRepo,
    checkModulesIntegrity,
    cloneRepo,
    removeRepoDir,
} from "../../../src/utils/release.utils";

const diffSummaryMock = jest.fn().mockResolvedValue({ changed: 1, deletions: 0, insertions: 0, files: ["test"] });
jest.mock("fs", () => {
    return {
        ...jest.requireActual("fs"),
        readFileSync: jest.fn().mockReturnValue(Buffer.from(JSON.stringify({ version: "0.0.1" }))),
    };
});

jest.mock("gluegun", () => {
    return {
        ...jest.requireActual("gluegun"),
        print: {
            info: jest.fn(),
            warning: jest.fn(),
            table: jest.fn(),
            highlight: jest.fn(),
            success: jest.fn(),
            colors: {
                blue: jest.fn(),
                green: jest.fn((text) => text + "(:green)"),
                red: jest.fn((text) => text + "(:red)"),
                bgYellow: jest.fn((text) => text),
                bgGreen: jest.fn((text) => text),
                bgRed: jest.fn((text) => text),
                muted: jest.fn((text) => text),
            },
        },
    };
});

jest.mock("@gitbeaker/node", () => {
    return {
        ...jest.requireActual("@gitbeaker/node"),
        Gitlab: jest.fn().mockImplementation(() => {
            return {
                Projects: {
                    show: jest.fn(() => ({ id: "1234", name: "test", web_url: "https://test.url.com", archived: false })),
                },
                Groups: {
                    projects: jest.fn(() => [
                        { id: "1", name: "test 1" },
                        { id: "2", name: "test 2" },
                    ]),
                },
            };
        }),
    };
});

jest.mock("simple-git", () => {
    return {
        ...jest.requireActual("simple-git"),
        simpleGit: jest.fn().mockImplementation(() => {
            return {
                clone: jest.fn(),
                checkout: jest.fn(),
                pull: jest.fn(),
                diffSummary: diffSummaryMock,
            };
        }),
    };
});

jest.mock("../../../src/constants/release-core-ids.constant", () => ["1"]);
jest.mock("../../../src/constants/release-module-ids.constant", () => []);

/**
 * Describe release.utils
 */
describe("UT release.utils", () => {
    const OLD_ENV = process.env;

    beforeEach(() => {
        process.env = { ...OLD_ENV, GITLAB_API_TOKEN: "token" };
    });

    afterEach(() => {
        jest.clearAllMocks();
        process.env = OLD_ENV;
    });

    describe("getGitlabRepo", () => {
        test("getGitlabRepo should return data form GitLab", async () => {
            const repo = await getGitlabRepo("17028326");
            expect(repo.id).toBe("1234");
            expect(repo.name).toBe("test");
        });
    });

    describe("cloneRepo", () => {
        test("cloneRepo should return data form GitLab", async () => {
            const repo = await getGitlabRepo("17028326");
            const clonedRepo = await cloneRepo(repo);
            expect(clonedRepo.id).toBe("1234");
            expect(clonedRepo.name).toBe("test");
            expect(clonedRepo.url).toBe("https://gitlab-ci-token:token@test.url.com");
            removeRepoDir(clonedRepo.dir);
        });
    });

    describe("checkModulesIntegrity", () => {
        test("checkModulesIntegrity should return missing repos", async () => {
            const missing = await checkModulesIntegrity();
            expect(missing).toEqual([{ id: "2", name: "test 2" }]);
        });
    });

    describe("check", () => {
        test("check should return repo has any changes", async () => {
            const repo = await getGitlabRepo("17028326");
            const clonedRepo = await cloneRepo(repo);
            const result = await check({ ...clonedRepo, url: repo.web_url });
            expect(result).toEqual({
                id: "1234",
                name: "test",
                webUrl: "https://test.url.com",
                someDiffs: true,
                diff: { changed: 1, deletions: 0, insertions: 0, files: ["test"] },
                changelog: "CHANGELOG.md was not found.",
            });
            removeRepoDir(clonedRepo.dir);
        });
    });

    // =============================================================================
    // printCommandHelp
    // =============================================================================
    describe("printCommandHelp", () => {
        test("printCommandHelp should print help for the command", async () => {
            printCommandHelp();
            expect(print.info).toHaveBeenCalledTimes(15);
        });
    });
});
