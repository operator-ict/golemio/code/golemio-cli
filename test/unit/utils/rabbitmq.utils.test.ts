import { print } from "gluegun";
import {
    printCommandHelp,
    printCommandHelpReceive,
    printCommandHelpSend,
    receiveData,
    sendData,
} from "../../../src/utils/rabbitmq.utils";

const publishMock = jest.fn(() => true);
const getMock = jest
    .fn()
    .mockResolvedValue({ content: Buffer.from("data") })
    .mockResolvedValueOnce({ properties: { headers: { test: true } }, content: Buffer.from("data with headers") })
    .mockResolvedValueOnce(undefined);

jest.mock("fs", () => {
    return {
        ...jest.requireActual("fs"),
        statSync: jest
            .fn()
            .mockReturnValue({ isDirectory: jest.fn().mockReturnValue(false) })
            .mockReturnValueOnce({ isDirectory: jest.fn().mockReturnValue(true) }),
        readFileSync: jest.fn().mockReturnValue(Buffer.from("file data")),
    };
});

jest.mock("gluegun", () => {
    return {
        ...jest.requireActual("gluegun"),
        print: {
            info: jest.fn(),
            warning: jest.fn(),
            table: jest.fn(),
            highlight: jest.fn(),
            success: jest.fn(),
            colors: { blue: jest.fn(), green: jest.fn((text) => text + "(:green)"), red: jest.fn((text) => text + "(:red)") },
        },
    };
});

jest.mock("amqplib", () => {
    return {
        ...jest.requireActual("amqplib"),
        connect: jest.fn().mockResolvedValue({
            createChannel: jest.fn(() => ({ on: jest.fn(), publish: publishMock, get: getMock })),
            on: jest.fn(),
            close: jest.fn(),
        }),
    };
});

/**
 * Describe rabbitmq.utils
 */
describe("UT rabbitmq.utils", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    // =============================================================================
    // sendData
    // =============================================================================
    describe("sendData", () => {
        test("sendData should send message with string data", async () => {
            await sendData({
                exchangeName: "test-dataplatform",
                queueKey: "manual.test-dataplatform.test.queue",
                messageOrPathToFile: "test message",
            });

            expect(print.warning).toHaveBeenCalledWith("File not found. Input sent as string.");
            expect(print.highlight).toHaveBeenCalledWith(
                `Message sent! Key: "manual.test-dataplatform.test.queue", messageOrFile: "test message"`
            );
            expect(publishMock).toHaveBeenCalledTimes(1);
            expect(publishMock).toHaveBeenCalledWith(
                "test-dataplatform",
                "manual.test-dataplatform.test.queue",
                Buffer.from("test message"),
                {
                    persistent: true,
                }
            );
        });

        test("sendData should send message with file data", async () => {
            await sendData({
                exchangeName: "test-dataplatform",
                queueKey: "manual.test-dataplatform.test.queue",
                messageOrPathToFile: "path/to/file",
            });

            expect(print.highlight).toHaveBeenCalledWith(
                `Message sent! Key: "manual.test-dataplatform.test.queue", messageOrFile: "path/to/file"`
            );
            expect(publishMock).toHaveBeenCalledTimes(1);
            expect(publishMock).toHaveBeenCalledWith(
                "test-dataplatform",
                "manual.test-dataplatform.test.queue",
                Buffer.from("file data"),
                {
                    persistent: true,
                }
            );
        });
    });

    // =============================================================================
    // receiveData
    // =============================================================================
    describe("receiveData", () => {
        test("receiveData should get one message from queue (with headers)", async () => {
            await receiveData({
                queueName: "test-dataplatform.test.queue",
            });

            expect(getMock).toHaveBeenCalledTimes(1);
            expect(getMock).toHaveBeenCalledWith("test-dataplatform.test.queue", { noAck: false });
            expect(print.highlight).toHaveBeenCalledTimes(2);
            expect(print.highlight).toHaveBeenCalledWith(`Headers:\n {\"test\":true}`);
            expect(print.highlight).toHaveBeenCalledWith(`Content:\n data with headers`);
        });

        test("receiveData should not get message (empty message)", async () => {
            await receiveData({
                queueName: "test-dataplatform.test.queue",
            });

            expect(getMock).toHaveBeenCalledTimes(1);
            expect(getMock).toHaveBeenCalledWith("test-dataplatform.test.queue", { noAck: false });
            expect(print.warning).toHaveBeenCalledTimes(1);
            expect(print.warning).toHaveBeenCalledWith("Queue is empty.");
        });

        test("receiveData should get one message from queue (no headers)", async () => {
            await receiveData({
                queueName: "test-dataplatform.test.queue",
            });

            expect(getMock).toHaveBeenCalledTimes(1);
            expect(getMock).toHaveBeenCalledWith("test-dataplatform.test.queue", { noAck: false });
            expect(print.highlight).toHaveBeenCalledTimes(2);
            expect(print.highlight).toHaveBeenCalledWith(`Headers:\n undefined`);
            expect(print.highlight).toHaveBeenCalledWith(`Content:\n data`);
        });

        test("receiveData should get and dequeue one message from queue", async () => {
            await receiveData({
                queueName: "test-dataplatform.test.queue",
                dequeue: true,
            });

            expect(getMock).toHaveBeenCalledTimes(1);
            expect(getMock).toHaveBeenCalledWith("test-dataplatform.test.queue", { noAck: true });
            expect(print.highlight).toHaveBeenCalledTimes(2);
            expect(print.highlight).toHaveBeenCalledWith(`Headers:\n undefined`);
            expect(print.highlight).toHaveBeenCalledWith(`Content:\n data`);
            expect(print.success).toHaveBeenCalledWith("Message was dequeued.");
        });
    });

    // =============================================================================
    // printCommandHelp
    // =============================================================================
    describe("printCommandHelp", () => {
        test("printCommandHelp should print help for the command", async () => {
            printCommandHelp();
            expect(print.info).toHaveBeenCalledTimes(7);
        });
    });

    // =============================================================================
    // printCommandHelpSend
    // =============================================================================
    describe("printCommandHelpSend", () => {
        test("printCommandHelpSend should print help for the command", async () => {
            printCommandHelpSend();
            expect(print.info).toHaveBeenCalledTimes(5);
        });
    });

    // =============================================================================
    // printCommandHelpReceive
    // =============================================================================
    describe("printCommandHelpReceive", () => {
        test("printCommandHelpReceive should print help for the command", async () => {
            printCommandHelpReceive();
            expect(print.info).toHaveBeenCalledTimes(4);
        });
    });
});
