[![pipeline status](https://gitlab.com/operator-ict/golemio/code/golemio-cli/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/golemio-cli/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/golemio-cli/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/golemio-cli/commits/master)

# Golemio CLI

> Golemio CLI is a collection of executables intended for use with Golemio services and modules

## Install the CLI

### Prerequisites

-   Node.js version 14 or later (https://nodejs.org)

### Install locally (recommended)

```bash
$ cd my-project

# With Yarn
$ yarn add --dev @golemio/cli
# For Portman support, run yarn add --dev @apideck/portman@^1.16.1

# With npm
$ npm install --save-dev @golemio/cli
# For Portman support, run npm install --save-dev @apideck/portman@^1.16.1

# Test it out
$ yarn golemio
$ npm exec golemio
```

### Install globally

```bash
# With Yarn
$ yarn global add @golemio/cli

# With npm
$ npm install -g @golemio/cli

# Test it out
$ golemio
```

## Commands summary

| Command                                 | Description                                                                                           |
| --------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| golemio                                 | Default command (welcome)                                                                             |
| golemio help                            | View available commands                                                                               |
| golemio migrate-db help                 | View available migration commands and flags                                                           |
| golemio migrate-db create \<name\>      | Create new migration scripts (--postgres, --path)                                                     |
| golemio migrate-db up                   | Execute up migrations (--postgres, --path)                                                            |
| golemio migrate-db down                 | Execute down migrations (--postgres, --path)                                                          |
| golemio migrate-db reset                | Reset all executed migrations (--postgres, --path)                                                    |
| golemio import-db-data help             | View available db data import commands and flags                                                      |
| golemio import-db-data                  | Import db data (--dangerous, --postgres)                                                              |
| golemio rabbitmq help                   | View available rabbitmq commands and flags                                                            |
| golemio rabbitmq send --help            | View available rabbitmq send flags                                                                    |
| golemio rabbitmq receive --help         | View available rabbitmq receive flags                                                                 |
| golemio release help                    | View available commands                                                                               |
| golemio release check-modules-integrity | Check module group if new repo was added                                                              |
| golemio release check                   | Check repos that are ready to release (--scope, --skip or --only, --interactive)                      |
| golemio release merge                   | Merge repos marked as release candidate by `check` command (--scope, --skip or --only, --interactive) |
| golemio release check-mr-pipelines      | Show pipelines of merged repos (--scope, --skip or --only, --interactive)                             |
| golemio release sync-branches           | Synchronize branches after release (--scope, --skip or --only, --interactive)                         |
| golemio release create-tags             | Create tags and releases of merged repos (--scope, --skip or --only, --interactive)                   |
| golemio release print-release-issue     | Print issues and release candidate modules                                                            |
| golemio swagger help                    | View available swagger commands and flags                                                             |
| golemio swagger generate                | Generate OAS file from multiple inputs                                                                |
| golemio swagger api-test                | Run Portman integration tests                                                                         |
| golemio asyncapi merge                  | Merge asyncapi documentation                                                                          |
| golemio asyncapi upload                 | Build and upload the asyncapi documentation to the azure blob storage                                 |

Read [release_management_CZ](./docs/release_management_CZ.md) for more info to `release` command. Read [migrations](./docs/migrations.md) for more info about database migrations.

## Environmental variables

| Variable                | Example                                       | Required                                             |
| ----------------------- | --------------------------------------------- | ---------------------------------------------------- |
| POSTGRES_CONN           | postgres://oict:oict-pass@localhost/oict-test | :white_check_mark: (mdb and idd postgres)            |
| RABBIT_CONN             | amqp://rabbit:pass@localhost                  | :white_check_mark: (rmq)                             |
| POSTGRES_MIGRATIONS_DIR | db/migrations/postgresql (default)            | :negative_squared_cross_mark:                        |
| SQL_DUMP_FILES          | db/example/\*.sql                             | :white_check_mark: (idd postgres)                    |
| GITLAB_API_TOKEN        | token                                         | :white_check_mark: (rls)                             |
| RELEASE_BRANCH_NAME     | release                                       | :negative_squared_cross_mark: (rls) default: release |
| TARGET_BRANCH_NAME      | master                                        | :negative_squared_cross_mark: (rls) default: master  |

The value of the variable GITLAB_API_TOKEN should be created in GitLab User Settings [page Access Tokens](https://gitlab.com/-/profile/personal_access_tokens). Token scopes are `api`, `read_repository` and `write_repository`.

## Development

### Local environment

To set up your local development environment, install project dependencies and create .env file in the root of the project (see [.env.template](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/development/.env.template) for reference)

```bash
$ npm install
$ cp .env.template .env
```

Now you can run the CLI in development mode, just type

```bash
$ ./bin/golemio.js
```

You don't need to compile the source code as the script automatically registers the [ts-node](https://www.npmjs.com/package/ts-node) loader in development mode

### Unit tests

Unit tests are located at test/unit. You can run them by typing

```bash
$ npm run test-unit
```

### Integration tests

Integration tests are located at test/integration. You can run them by typing

```bash
# Run local services, recommended
$ docker-compose up -d

$ npm run test-integration
```

## License

[MIT](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/development/LICENSE)
